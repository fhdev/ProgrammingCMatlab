function sz = l_kombi( n,m )
% n alatt az m kombin�ci� rekurz�van
% Ha m==0, vagy n==m, akkor 1
% egy�bk�nt, a felette lev� kett� �sszege
if m==0 || m==n
    sz=1;
else
    sz=kombi(n-1,m-1)+kombi(n-1,m);
end
end


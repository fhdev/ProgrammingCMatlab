#include <stdio.h>
#include <stdlib.h>

// Magyar �kezetek
#include<locale.h>
#include<windows.h>

#define DB 10

int main(int argc, char *argv[])
{
    // Magyar �kezetek
    setlocale(LC_ALL, "");
    SetConsoleCP(1250u);
    SetConsoleOutputCP(1250u);

    // Program
    printf("Pointer aritmetika.\n");

    int *tomb = malloc(DB * sizeof(int));

    for(int i = 0; i < DB; i++)
    {
        tomb[i] = i;
    }

    for(int i = 0; i < DB; i++)
    {
        printf("tomb + %d = %p\t", i, tomb + i);
        printf("tomb[%d] = %d\t", tomb[i], i);
        printf("%d[tomb] = %d\t", i[tomb], i);
        printf("*(tomb + %d) = %d\n", *(tomb + i), i);
    }

    free(tomb);

    return 0;
}

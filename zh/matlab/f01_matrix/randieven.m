% Random m�trix gener�l�sa (n x m) m�retben, mely csak p�ros elemekb�l �ll.
function A = randieven(imax, n, m)
A = 2 * randi(imax, n, m);
end
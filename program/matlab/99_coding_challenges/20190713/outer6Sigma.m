% Create a MATLAB function that counts the percentage of elements being outside the ExpectedValue 
% +- StandardDevitation range in a numeric variable with any size and dimensions.
function p = outer6Sigma(v)
if ~isnumeric(v)
    error('MATLAB:InvalidArgument', 'The parameter should be a numeric varaible.');
end
% Serialize variable
v = v(:);
% Do calculation
m = mean(v);
s = std(v);
p = sum((v > (m + s)) | (v < (m - s))) / length(v);
end
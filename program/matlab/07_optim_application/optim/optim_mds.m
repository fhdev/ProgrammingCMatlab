% Find parameters of a mass-damper-spring system with unconsrained optimization
%% Clear workspace
clear;
close all;
clc;
%% Parameters as global variables
global m d s;
%% Load input and measurement
% Load default parameters and input
mds_param;
mds_input;
% Load measurement
load('meas.mat');
%% Create function handles for simulation
% Function handle of system.
sysfcn = @(t, X) mds_fcn(t, X, t_in, u_in);
% Options for ODE solution.
odeopts = odeset('MaxStep', t_step, 'InitialStep', t_step);
% Function handle of ODE solution.
odefcn = @() ode45(sysfcn, [t_start, t_stop], X_0, odeopts);
%% Simulate model with initial guess of parameter values
% Initial value of parameters p = [m; d; s].
p_0 = [1; 1.5; 1];
% Calculate initial values based on time-domain response
if false
    estim_mds; %#ok<UNRCH>
    p_0 = [m_meas, d_meas, s_meas];
end
% Simulate model with initial guess
m = p_0(1);
d = p_0(2);
s = p_0(3);
[t_p_0, X_p_0] = odefcn();
%% Find parameters of original model via optimalization
% Function handle of FMINSEARCH optimalization.
optimfcn = @(p) mds_costfcn(p, odefcn, t_out_0, X_meas);
% Options for FMINSEARCH optimalization.
optimopts = optimset('Display',       'Iter',         ... 
                     'MaxFunEvals',   500,            ...
                     'MaxIter',       500,            ...
                     'PlotFcns',      @optimplotfval, ...
                     'TolFun',        1e-6,           ...
                     'TolX',          1e-3);
% Search for the parameter values at which the output of the system fits best to the measurement.
% Run optimization
tic;
[p_opt, c_opt] = fminsearch(optimfcn, p_0, optimopts);
toc;
%% Display results
% Display final parameter values
fprintf('m = %f, d = %f, s = %f\n', m, s, d);
% Calculate response with optimized parameters.
[t_out, X_out] = odefcn();
% Plot results
plot_optim;
% Parameters of a mass-damper-spring system
% Mark parameter values as global variables
global m d s;
% Parameter values
m = 1;    % mass [kg]
d = 1;    % damping constant [Ns/m]
s = 1;    % spring constant [N/m]

function [ steps ] = hanoi_lep( disks )
%HANOI_LEP gives the required stepcount for a given number of disks
if disks==1
    steps=1;
else
    steps=2*hanoi_lep(disks-1)+1;
end
end


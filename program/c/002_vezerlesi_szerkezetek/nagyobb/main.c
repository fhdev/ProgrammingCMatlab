#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    double szam1, szam2;
    printf("Kerem adja meg az elso szamot!\n");
    printf("szam1 = ");
    scanf("%lf", &szam1);
    printf("Kerem adja meg a masodik szamot!\n");
    printf("szam2 = ");
    scanf("%lf", &szam2);

    if(szam1 == szam2)
    {
        printf("A ket szam egyenlo: %.10g = %.10g .\n", szam1, szam2);
    }
    else if(szam1 > szam2)
    {
        printf("Az elso szam nagyobb: %.10g > %.10g .\n", szam1, szam2);
    }
    else
    {
        printf("A masodik szam nagyobb: %.10g < %.10g .\n", szam1, szam2);
    }



    return 0;
}

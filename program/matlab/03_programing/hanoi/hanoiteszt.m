function []=hanoiteszt(n)
% hanoiteszt f�ggv�ny, amely �tvesz egy korongsz�mot, �s lej�tssza a j�t�kot
global towers; %#ok<NUSED>
inithanoi(n);
drawhanoi(1,1,1);
hanoi(n,1,2,3);
% A sin(x) �rt�k�nek kisz�m�t�sa Taylor-sorral.
function s = sint(x, nmax)
n = 0:nmax;
s = sum((-1).^n ./ factorial(2*n + 1) .* x.^(2 * n + 1));
end
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DARAB       20
#define X            0
#define Y            1
#define R            2
#define PARAMSZAM    3
#define RMAX       100
#define TRUE         1
#define FALSE        0

double random(double also, double felso);
void general(double korok[][PARAMSZAM], double maxSugar, int nKorok);
int ellenoriz(double korok[][PARAMSZAM], int nKorok);

int main()
{
    int darab;
    double korok[DARAB][PARAMSZAM];
    srand(time(0));
    printf("Veletlen generalt korok:\n    %11s%11s%11s\n", "x", "y", "r");
    general(korok, RMAX, DARAB);
    printf("A generalt korok tartalmazzak-e az origot:\n");
    darab = ellenoriz(korok, DARAB);
    printf("Osszesen %d korbol %d tartalmazza az origot.\n", DARAB, darab);
    getchar();
    return 0;
}

double random(double also, double felso)
{
    return also + ((double)rand() / (double)RAND_MAX) * (felso - also);
}

void general(double korok[][PARAMSZAM], double maxSugar, int nKorok)
{
    int iKor;
    for (iKor = 0; iKor < nKorok; iKor++)
    {
        int iParam;
        printf("%3d: ", iKor);
        for (iParam = 0; iParam < PARAMSZAM; iParam++)
        {
            double min = -maxSugar;
            if (iParam == R)
            {
                min = 0;
            }
            korok[iKor][iParam] = random(min, maxSugar);
            printf("%+10.5lf ", korok[iKor][iParam]);
        }
        printf("\n");
    }
}

int ellenoriz(double korok[][PARAMSZAM], int nKorok)
{
    int darab = 0;
    int iKor;
    for (iKor = 0; iKor < nKorok; iKor++)
    {
        if(korok[iKor][R] * korok[iKor][R] >= korok[iKor][X] * korok[iKor][X] + korok[iKor][Y] * korok[iKor][Y])
        {
            darab++;
            printf("%3d: Benne van az origo!\n", iKor);
        }
        else
        {
            printf("%3d: Nincs benne az origo!\n", iKor);
        }
    }
    return darab;
}

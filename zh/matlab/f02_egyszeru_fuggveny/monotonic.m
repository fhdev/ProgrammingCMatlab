% Monotonit�s vizsg�lat. Visszat�r�si �rt�k: 1, ha monoton n�v�, -1, ha monoton cs�kken�, egy�bk�nt
% 0.
function m = monotonic(x)
m = 0;
d = diff(x);
if all(d >= 0)
    m = 1;
elseif all(d <= 0)
    m = -1;
end
end
clear;
close all;
%% Sepci�lis m�trixok el��ll�t�sa

% Random m�trix gener�l�sa (n x m) m�retben, mely csak p�ratlan elemekb�l �ll.
A = randiodd(100, 3, 4);
disp(A);

% Random m�trix gener�l�sa (n x m) m�retben, mely csak p�ros elemekb�l �ll.
A = randieven(100, 3, 4);
disp(A);

% M�trix el��ll�t�sa (n x m) m�retben, melynek minden sor�ban az 1, 2, ..., m sz�mtani sorozat
% tal�lhat�.
A = arprogmatr(4, 5);
disp(A);

% M�trix el��ll�t�sa (n x m) m�retben, melynek minden oszlop�ban a Fibonacci-sorozat 1, 2, ..., n
% elemei tal�lhat�k.
A = fibomatr(4, 5);
disp(A);

% Random m�trix gener�l�sa (n x m) m�retben, melynek elemei az 1, 2, ..., (n x m) sz�mtani sorozat
% tagjai
A = randpermmat(2, 6);
disp(A);

% M�trix el��ll�t�sa (n x n) m�retben, melynek f��tl�j�ban x, mell�k�tl�j�ban y �rt�kek vannak. A
% f�- �s mell�k�tl�ban is szerepl� elem �rt�ke x + y legyen.
A = diagonal2(4, 2, 5);
disp(A);

% M�trix el��ll�t�sa (n x n) m�retben, melynek f��tl�j�ban az 1!, 2!, ..., n! faktori�lis �rt�kek
% tal�lhat�ak.
A = factdiag(3);
disp(A);

% M�trix el��ll�t�sa (n x n) m�retben, melynek mell�k�tl�j�ban az 1, 3, ... 2n-1 �rt�kek tal�lhat�ak.
A = antidiagodd(3);
disp(A);

% Fels� h�romsz�g m�trix el��ll�t�sa (n x m) m�retben, melyben minden nem 0 �rt�k x. 
A = triux(3, 4, 11);
disp(A);

% Als� h�romsz�g m�trix el��ll�t�sa (n x m) m�retben, melyben minden nem 0 �rt�k x. 
A = trilx(3, 4, 11);
disp(A);

% Random m�trix gener�l�sa (n x n) m�retben, melynek minden sor�ban �s oszlop�ban pontosan 1 db.
% elem 1, a t�bbi elem pedig 0.
A = snake(4);
disp(A);

% M�trix gener�l�sa (n x n) m�retben, melynek minden a_ij eleme 1, ha i �s j egyszerre p�ratlan vagy
% p�ros (sakkt�bla m�trix).
A = chesstable(5);
disp(A);

% M�trix gener�l�sa (n x n) m�retben, melynek minden a_ij eleme x, ha i p�ros �s j p�ratlan, vagy ha
% i p�ratlan, �s j p�ros.
A = tiles(5, 10);
disp(A);

% M�trix el��ll�t�sa (n x m) m�retben, melynek minden a_ij elem�nek �rt�ke az indexeinek �sszege
% (a_ij = i + j).
A = indmat(4, 5);
disp(A);

% M�trix el��ll�t�sa (n x m) m�retben, melyben az 1, 2, ..., (n x m) sz�mtani sorozat tagjai vannak
% oszlopfolytonosan.
A = omat(7, 3);
disp(A);

% M�trix el��ll�t�sa (n x m) m�retben, melyben az 1, 2, ..., (n x m) sz�mtani sorozat tagjai vannak
% sorfolytonosan.
A = smat(7, 3);
disp(A);

% Random szimmetrikus m�trix el��ll�t�sa (n x n) m�retben a_ij : [0; 1]. 
A = symmat(5);
disp(A);

% M�trix el��ll�t�sa (n x n) m�retben, melynek f��tl�j�ban a n�gyzetsz�mok tal�lhat�ak.
A = squarediag(5);
disp(A);

% M�trix n x m m�retben, melynek elemei az 1, 2, ..., n * m sz�mtani sorozat tagjai, v�letlenszer�en
A = randpermmat(4, 5);
disp(A);
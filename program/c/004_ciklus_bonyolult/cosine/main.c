#define _USE_MATH_DEFINES
#include <stdio.h>
#include <math.h>

int main(int argc, char *argv[])
{
	// A pontosság, a lépésköz, illetve a változó beolvasása
	printf("A cos(alpha) ertekenek kiszamitasa nmax + 1 lepesben vagy epsilon pontossaggal.\n");

	printf("Kerem adja meg a lepesek maximalis szamat: nmax = ");
	int nmax;
	scanf("%d",&nmax);

	printf("Kerem adja meg a megkivant pontossagot: epsilon = ");
	double epsilon;
	scanf("%lf",&epsilon);

	printf("Kerem adja meg a szog erteket [fok]: alpha = ");
	double alpha;
	scanf("%lf",&alpha);

	// Egész fordulatok levonása és átváltás radiánba
	double x = alpha - 360.0 * (double)((int)(alpha / 360.0));
	x *= M_PI / 180.0;

	// Inicializálás n = 0 esetére
	// Az x^(2n) értékét tároló változó
	double pow = 1.0;
	// A (2n)! értékét tároló változó
	double fact = 1.0;
	// A (-1)^n értékét tároló változó
	int sgn = 1.0;

	// A sin értékét tároló változó
	double cos = 0.0;

	// Öszeg számítása
	int n;
	for(n = 0; n <= nmax; n++)
	{
		// A közelítõ összeg legújabb tagjának számítása
		double delta = sgn / fact * pow;
		// Pontosság ellenõrzése (fabs és makró nélkül :O)
		if(delta * delta <= epsilon * epsilon)
		{
			// Break esetén nem hajtóda végre a for belsejébe írt inkrementálás
			n++;
			break;
		}
		// A közelítõ összeg kiszámítása
		cos += delta;

		// (2n+1)! kiszámítása a következő körre
		fact *= (2 * (n + 1) - 1) * (2 * (n + 1));
		// x^(2n+1) kiszámítása a következő körre
		pow *= x * x;
		// (-1)^n kiszámítása a következő körre
		sgn *= -1;
	}
	printf("cos(%.6g [fok]) = cos(%.6g [rad]) = %.6g\n", alpha, x, cos);
	printf("Szamitva %d lepesben.\n", n);
	return 0;
}

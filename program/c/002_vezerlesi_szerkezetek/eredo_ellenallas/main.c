#include <stdio.h>
#include <ctype.h>

int main(int argc, char *argv[])
{
    printf("Eredo ellenallas szamitasa.\n");
    printf("Soros [S] vagy parhuzamos [P] kapcsolast szeretne szamolni? ");
    char kapcsolas;
    scanf("%c", &kapcsolas);
    kapcsolas = toupper(kapcsolas);
    if((kapcsolas == 'S') || (kapcsolas == 'P'))
    {
        double R = 0, Re = 0;
        while(1)
        {
            printf("R = ");
            scanf("%lf", &R);
            if (R <= 0)
            {
                break;
            }
            switch(kapcsolas)
            {
				case 'S':
				{
					Re += R;
					break;
				}
				case 'P':
				{
					Re += 1 / R;
					break;
				}
            }
        }
        if((kapcsolas == 'P') && (Re > 0))
        {
            Re = 1 / Re;
        }
        printf("Az eredmeny: Re = %.4lf\n", Re);
    }
    else
	{
		printf("Ilyen kapcsolas nem letezik!\n");
	}
    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MEGOLDAS_2

#define DARAB       10
#define OLDALSZAM    3
#define LEGROVIDEBB  0
#define LEGHOSSZABB 10
#define TRUE         1
#define FALSE        0

double random(double also, double felso);
void general(double oldalhosszak[][OLDALSZAM], double legrovidebb, double leghosszabb, int nHaromszogek);
int ellenoriz(double oldalhosszak[][OLDALSZAM], int nHaromszogek);
int ellenoriz2(double oldalhosszak[][OLDALSZAM], int nHaromszogek);

int main()
{
    int darab;
    double oldalhosszak[DARAB][OLDALSZAM];
    srand(time(0));
    printf("Veletlen generalt haromszog oldalhosszak:\n");
    general(oldalhosszak, LEGROVIDEBB, LEGHOSSZABB, DARAB);
    printf("A generalt oldalhosszak haromszoget alkotnak-e:\n");
    darab = ellenoriz(oldalhosszak, DARAB);
    printf("Osszesen %d oldalharmasbol %d alkot haromszoget.\n", DARAB, darab);
    darab = ellenoriz2(oldalhosszak, DARAB);
    printf("Osszesen %d oldalharmasbol %d alkot haromszoget.\n", DARAB, darab);
    getchar();
    return 0;
}

double random(double also, double felso)
{
    return also + ((double)rand() / (double)RAND_MAX) * (felso - also);
}

void general(double oldalhosszak[][OLDALSZAM], double legrovidebb, double leghosszabb, int nHaromszogek)
{
    int iHaromszog;
    for (iHaromszog = 0; iHaromszog < nHaromszogek; iHaromszog++)
    {
        int iOldal;
        printf("%3d: ", iHaromszog);
        for (iOldal = 0; iOldal < OLDALSZAM; iOldal++)
        {
            oldalhosszak[iHaromszog][iOldal] = random(legrovidebb, leghosszabb);
            printf("%+10.5lf ", oldalhosszak[iHaromszog][iOldal]);
        }
        printf("\n");
    }
}

int ellenoriz(double oldalhosszak[][OLDALSZAM], int nHaromszogek)
{
    int darab = 0;
    int iHaromszog;
    for (iHaromszog = 0; iHaromszog < nHaromszogek; iHaromszog++)
    {
        unsigned char haromszog = TRUE;
        int iOldal;
        for (iOldal = 0; iOldal < OLDALSZAM; iOldal++)
        {
            int iMasikOldal = (iOldal + 1) % OLDALSZAM;
            int iHarmadikOldal = (iOldal + 2) % OLDALSZAM;
            if(oldalhosszak[iHaromszog][iOldal] >= oldalhosszak[iHaromszog][iMasikOldal] + oldalhosszak[iHaromszog][iHarmadikOldal])
            {
                haromszog = FALSE;
                break;
            }
        }
        if(haromszog)
        {
            printf("%3d: HAROMSZOG!\n", iHaromszog);
            darab++;
        }
        else
        {
            printf("%3d: NEM HAROMSZOG!\n", iHaromszog);
        }
    }
    return darab;
}

int ellenoriz2(double oldalhosszak[][OLDALSZAM], int nHaromszogek)
{
    int darab = 0;
    int iHaromszog;
    for (iHaromszog = 0; iHaromszog < nHaromszogek; iHaromszog++)
    {
        int iLegnagyobb = 0;
		int iMasikOldal;
		int iHarmadikOldal;
        int iOldal;
        for(iOldal = 1; iOldal < OLDALSZAM; iOldal++)
		{
			if(oldalhosszak[iHaromszog][iOldal] > oldalhosszak[iHaromszog][iLegnagyobb])
			{
				iLegnagyobb = iOldal;
			}
		}
		iMasikOldal = (iLegnagyobb + 1) % OLDALSZAM;
		iHarmadikOldal = (iLegnagyobb + 2) % OLDALSZAM;
		if(oldalhosszak[iHaromszog][iLegnagyobb] < oldalhosszak[iHaromszog][iMasikOldal] + oldalhosszak[iHaromszog][iHarmadikOldal])
		{
			darab++;
			printf("%3d: HAROMSZOG!\n", iHaromszog);
		}
		else
		{
			printf("%3d: NEM HAROMSZOG!\n", iHaromszog);
		}
    }
    return darab;
}

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
// Magyar �kezetekhez
#include <locale.h>
#include <windows.h>

#define FALSE 0
#define TRUE 1

#define MIN 1
#define MAX 90
#define DB 5

void beolvas(int darab, int also, int felso, int szamok[]);
int randi(int also, int felso);
void sorsol(int darab, int also, int felso, int szamok[]);
void kiir(int darab, int szamok[]);
void rendez(int darab, int szamok[]);
int rendezett_talalatot_szamol(int darab, int megadott[], int sorsolt[]);

int main(int argc, char *argv[])
{
	// Magyar �kezetekhez
    setlocale(LC_ALL, "");
    SetConsoleCP(1250);
    SetConsoleOutputCP(1250);

    // Program
    printf("Lott� j�t�k.\n");
    int megadott[DB];
    beolvas(DB, MIN, MAX, megadott);
    rendez(DB, megadott);
    int sorsolt[DB];
    sorsol(DB, MIN, MAX, sorsolt);
    rendez(DB, sorsolt);
    printf("Az �n sz�mai:\n");
    kiir(DB, megadott);
    printf("A sorsolt sz�mok:\n");
    kiir(DB, sorsolt);
    int talalatok = rendezett_talalatot_szamol(DB, megadott, sorsolt);
    printf("A tal�latok sz�ma: %d\n", talalatok);
    return 0;
}

unsigned char puffert_torol()
{
	int maradek;
	unsigned char volt_torles = FALSE;
	while (((maradek = getchar()) != '\n') && (maradek != EOF))
	{
		volt_torles = TRUE;
	}
	return volt_torles;
}

void beolvas(int darab, int also, int felso, int szamok[])
{
    printf("K�rem adjon meg %d darab eg�sz sz�mot %d �s %d k�z�tt!\n", darab, also, felso);
    for(int iszam = 0; iszam < darab; iszam++)
    {
        // �j sz�m beolvas�sa
        printf("%d. sz�m: ", iszam + 1);
        scanf("%d", &szamok[iszam]);
        // A bemeneti puffer marad�k�nak t�rl�se
        unsigned char volt_torles = puffert_torol();
        // �rv�nyess�g ellen�rz�se
        if(volt_torles)
        {
            iszam--;
            printf("�rv�nytelen bemenet! K�rem pr�b�lja �jra!\n");
        }
        else if((szamok[iszam] > felso) || (szamok[iszam] < also))
        {
            iszam--;
            printf("A sz�m nem a megfelel� tartom�nyba esik. K�rem v�lasszon m�sikat!\n");
        }
        else
        {
            for(int ielozo = 0; ielozo < iszam; ielozo++)
            {
                if(szamok[iszam] == szamok[ielozo])
                {
                    iszam--;
                    printf("Egy sz�mot csak egyszer adhat meg! K�rem v�lasszon m�sikat!\n");
                    break;
                }
            }
        }
    }
}


int randi(int also, int felso)
{
	return also + rand() % (felso - also + 1);
}

void sorsol(int darab, int also, int felso, int szamok[])
{
	srand(time(0));
	rand();
	for(int iuj = 0; iuj < darab; iuj++)
	{
	    szamok[iuj] = randi(also, felso);
        for(int ielozo = 0; ielozo < iuj; ielozo++)
        {
            if(szamok[iuj] == szamok[ielozo])
            {
                iuj--;
                break;
            }
        }
	}
}

void kiir(int darab, int szamok[])
{
	for(int iszam = 0; iszam < darab; iszam++)
	{
		printf("(%2d) ", szamok[iszam]);
	}
	printf("\n");
}

void rendez(int darab, int szamok[])
{
	for(int iutolso = (darab - 1); iutolso >= 0; iutolso--)
	{
		for(int iszam = 0; iszam < iutolso; iszam++)
		{
			if (szamok[iszam] > szamok[iszam + 1])
			{
				int seged = szamok[iszam];
				szamok[iszam] = szamok[iszam + 1];
				szamok[iszam + 1] = seged;
			}
		}
	}
}

int rendezett_talalatot_szamol(int darab, int megadott[], int sorsolt[])
{
	int talalatok = 0;
	for(int imegadott = 0; imegadott < darab; imegadott++)
	{
	    for(int isorsolt = 0; isorsolt < darab; isorsolt++)
        {
            if(megadott[imegadott] == sorsolt[isorsolt])
            {
                talalatok++;
            }
        }
	}
	return talalatok;
}

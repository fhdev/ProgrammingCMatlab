#include <stdio.h>
#include <stdlib.h>

int main()
{
	printf("Az n! ertekenek kiszamitasa.\n");
    printf("Kerem adja meg n erteket!\nn = ");
    int n;
    scanf("%d", &n);
    int f = 1;
    int szam = 1;
    while(szam <= n)
	{
		f = f * szam;
		szam = szam + 1;
	}
	printf("%d! = %d\n", n, f);
    return 0;
}

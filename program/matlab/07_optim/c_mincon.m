% Constrained optimization of Rosenbrock function with fmincon in a circular area
%% Clear workspace
clear;
close all;
clc;
%% Rosenbrock function
rsb = @(a, b, x, y) (a - x).^2 + b * (y - x.^2).^2;
rsbp = @(x, y) rsb(1, 100, x, y);
%% Evaluate function on a grid
% Resolution of grid
r = 0.5;
% Limits of grid.
lim = 10;
% Create grid to evaluate on.
[x, y] = meshgrid(-lim:r:lim,-lim:r:5 * lim);
% Evaluate function on grid.
z = rsbp(x, y);
%% Evaluate constraint function at the edge of constrained area
% The place of the minima must be in a circle (radius, origo coordinates)
r = 3;      
xo = 2;
yo = 40;
% Evaluate function values
t = 0:0.1:2.1*pi;
xc = r * sin(t) + xo;
yc = r * cos(t) + yo;
zc = rsbp(xc, yc);
%% Run optimization
opts = optimoptions('fmincon',...
                    'Algorithm', 'interior-point',...
                    'Display', 'iter',...
                    'OptimalityTolerance', 1e-6);
X0 = [0, 0];
% Cost function handle that replaces the two arguments (x,y) with one vector argument (X)
f = @(X) rsbp(X(1), X(2));
% Constraint function handle ((x-x_o)^2 + (y-y_o)^2 <= r^2) that replaces the two arguments (x, y)
% with one vector argument (X) and fills arguments that are only parameters of the constraint
% function
c = @(X) con(X(1), X(2), r, xo, yo);
% Run optimization
[Xm, zm] = fmincon(f, X0, [], [], [], [], [], [], c, opts);
%% Plot results
% Plot results of constrained optimization of Rosenbrock function in a circular area with fmincon
red     = [1, 0, 0];
green   = [0, 1, 0];
blue    = [0, 0, 1];
white   = [1, 1, 1];
figure('Name', 'mincon', 'Unit', 'Centimeters', 'Position', [0, 0, 14, 10], 'Color', white);
hold on;
% Plot surface of Rosenbrock function.
surf(x, y, z, 'EdgeColor', 'Interp', 'FaceColor', 'Interp');
% Plot the edge of the circular are, in which the minimum was searched.
plot3(xc, yc, zc, 'Color', red, 'LineWidth', 3);
% Plot the constrainted minima.
scatter3(Xm(1), Xm(2), zm, 'MarkerEdgeColor', green, 'MarkerFaceColor', green);
title('Constrained minimization in a circular area', 'Interpreter', 'Latex', 'HorizontalAlignment', 'Center');
xlabel('$x$', 'Interpreter', 'Latex');
ylabel('$y$', 'Interpreter', 'Latex');
zlabel('$z$', 'Interpreter', 'Latex');
set(gca, 'TickLabelInterpreter', 'Latex', 'FontSize', 12);
grid on;
box on;
view([-20, 47]);

%% Constraint function
% (x-x_o)^2 + (y-y_o)^2 <= r^2)
function [c, ceq] = con(x, y, r, xo, yo)
    c = (x - xo)^2 + (y - yo)^2 - r^2;
    ceq = [];
end
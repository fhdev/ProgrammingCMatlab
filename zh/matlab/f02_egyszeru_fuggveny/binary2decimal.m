function decimal = binary2decimal(binary)
len = length(binary);
decimal = 0;
n = 1;
for i = len : -1 : 1
    decimal = decimal + n * binary(i);
    n = n * 2;
end
end
clear;
close all;
% Parameterek
c1 = 2; % [N/m]
c2 = 3; % [N/m]
m = 20; % [kg]
k = 24; % [Ns/m]
% Sz�m�t�s ideje �s kezdeti �rt�k
ts = [0; 25];
X0 = [0; 0];
% Be�ll�t�sok
o = odeset('MaxStep', 1e-3);
% Mozg�segyenlet megold�sa
[t, X] = ode45(@(t, X)rendszer(t, X, c1, c2, k, m), ts, X0, o);
% Ellen�rz�s
% Ir�ny�t�stechnika 2 feladatgy�jtem�ny 1.2.11 mechanikai rendszer 
sys = tf([k, c1], [m, k, c1 + c2]);
[y, t2] = step(sys);
% Eredm�nyek megjelen�t�se
figure;
subplot(2,1,1);
plot(t, X(:, 1), 'b-');
title('output derivative');
xlabel('t');
ylabel('dy');
subplot(2,1,2);
hold on;
plot(t2, y, 'r:', 'LineWidth', 2);
plot(t, X(:, 2), 'b-');
title('output');
xlabel('t');
ylabel('y');
legend('step', 'ode');

%% Ir�ny�t�stechnika 2 feladatgy�jtem�ny 1.2.11 mechanikai rendszer 
function dX = rendszer(t, X, c1, c2, k, m)
% Bemenet
u = min(10*t, 1);
du = 10 * (t < 0.1);
% Deriv�lt sz�m�t�sa
A = [-k/m, -(c1+c2)/m;
        1,          0];
B = [c1/m;
        0];
B2 = [k/m;
        0];
dX = A * X + B * u + B2 * du;
end
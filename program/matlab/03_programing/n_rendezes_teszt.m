nmeret = 20;
nfutas = 10;
meretek = floor(linspace(1e1, 1e4, nmeret));

idokBuborek = zeros(size(meretek));
idokGyors = zeros(size(meretek));

for imeret = 1 : nmeret
    tomb = rand(1, meretek(imeret));
    for ifutas = 1 : nfutas
        tic;
        tombBuborek = h_buborekrendezes(tomb);
        idokBuborek(imeret) = idokBuborek(imeret) + toc;
        tic;
        tombGyors = m_gyorsrendezes(tomb, 1, meretek(imeret));
        idokGyors(imeret) = idokGyors(imeret) + toc;
    end
    fprintf('%d/%d k�sz\n', imeret, nmeret);
end

idokBuborek = idokBuborek / nfutas;
idokGyors = idokGyors / nfutas;

figure('Name', 'Rendez�sek', 'Color', 'White');
hold on;
plot(meretek, idokBuborek);
plot(meretek, idokGyors);
legend('bubor�k', 'gyors');
xlabel('t�mb m�rete');
ylabel('rendez�si id� (m�sodperc)');
grid on;
box on;
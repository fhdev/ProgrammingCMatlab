#include <stdio.h>

int main(int argc, char *argv[])
{
	double szam1=0.0;
	double szam2=0.0;
    printf("Ket szam osszeadasa.\n");
    printf("Kerem adja meg az elso szamot!\n");
	scanf("%lf", &szam1);
    printf("Kerem adja meg a masik szamot!\n");
	scanf("%lf", &szam2);
	printf("%lf + %lf = %lf\n", szam1, szam2, szam1+szam2);
	getchar();
    return 0;
}

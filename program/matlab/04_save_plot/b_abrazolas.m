close('all');
%% Adatok
t = 0 : 0.01 * pi : 2 * pi;
s1 = sin(t);
s2 = 2 * sin(0.5 * t + pi / 8);
s3 = 0.5 * sin(2 * t - pi / 6);
c1 = cos(t);
%% �br�zol�s
szelesseg = 8;
magassag = 5;
betumeret = 8;
fig = figure('Name', 'Szinuszok', 'Color', 'White', 'Unit', 'Centimeters', ...
    'Position', [0, 0, szelesseg, magassag], ...
    'PaperPosition', [0, 0, szelesseg, magassag], ...
    'PaperSize', [szelesseg, magassag]);
ax = axes(fig, 'FontSize', betumeret, 'FontWeight', 'Normal', ...
    'TitleFontWeight', 'Normal', 'TitleFontSizeMultiplier', 1.0, ...
    'LabelFontSizeMultiplier', 1.0, 'Box', 'on');
% egym�s ut�ni plot parancsok egym�ssal egy�tt jelenjenek meg
hold(ax, 'on');
% h�l�
grid(ax, 'on');
% egyes g�rb�k kirajzol�sa
plot(ax, t, s1, 'Color', [231, 76, 60] / 255, 'LineStyle', '-', 'LineWidth', 1);
plot(ax, t, s2, 'Color', [39, 174, 96] / 255, 'LineStyle', ':', 'LineWidth', 2);
% tengelyfeliratok m�dos�t�sa
ax.XTick = [0, pi/2, pi, 3*pi/2, 2*pi];
ax.XTickLabel = {'0', '\pi/2', '\pi', '3\pi/2', '2\pi'};
% feliratok
title(ax, 'Szinuszhull�mok');
xlabel(ax, 't [s]');
ylabel(ax, 'A [m]');
legend(ax, {'sin(t)', '2sin(0.5t+pi/8)'}, 'Location', 'Best', ...
    'Orientation', 'Vertical', 'FontSize', betumeret);
%% Ment�s
%     f�jln�v      form�tum  renderel�
% vektorgrafika
print('abra.emf', '-dmeta', '-painters');
print('abra.pdf', '-dpdf', '-painters');
% pixelgrafika
print('abra.png', '-dpng', '-opengl');

function x = i_fibo(n)
if n < 0
    disp('Érvénytelen n értéke!');
elseif (n == 0) || (n == 1)
    x = n;
else
    x = fibo(n-1) + fibo(n-2);
end
end
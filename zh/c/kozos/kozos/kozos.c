#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 1000
#define MIN -100
#define MAX 100
#define EGYSORBA 75

int feltolt(int tomb[], int n, int min, int max)
{
	for (int i = 0; i < n; i++)
	{
		tomb[i] = (rand() % (max - min + 1)) + min;
	}
	return n;
}

int kozos_szamol(int egyik[], int masik[], int kozos[], int n)
{
	int szamlalo = 0;
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			if (egyik[i] == masik[j])
			{
				unsigned char uj = 1;
				for (int k = 0; k < szamlalo; k++)
				{
					if (egyik[i] == kozos[k])
					{
						uj = 0;
						break;
					}
				}
				if (uj)
				{
					kozos[szamlalo] = egyik[i];
					szamlalo++;
					break;
				}
			}
		}
	}
	return szamlalo;
}

void kiir(int tomb[], int n, int egysorba)
{
	int karakterek = 0;
	for (int i = 0; i < n; i++)
	{
		karakterek += printf("%+4d ", tomb[i]);
		if (karakterek >= egysorba)
		{
			printf("\n");
			karakterek = 0;
		}
	}
}

int main()
{
	srand(time(0));
	printf("Adja meg az elemek szamat! n = ");
	int n;
	scanf("%d", &n);
	if ((n > N) || (n < 1))
	{
		printf("Az elemek szama maximum %d lehet!", N);
		return -1;
	}
	int egyik[N], masik[N], kozos[N];
	feltolt(egyik, n, MIN, MAX);
	feltolt(masik, n, MIN, MAX);
	int k = kozos_szamol(egyik, masik, kozos, n);
	printf("\nAz elso tomb:\n");
	kiir(egyik, n, EGYSORBA);
	printf("\nA masodik tomb:\n");
	kiir(masik, n, EGYSORBA);
	printf("\nA kozos elemek szama: %d\nA kozos elemek:\n", k);
	kiir(kozos, k, EGYSORBA);
	return 0;
}

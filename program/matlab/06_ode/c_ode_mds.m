%% Solution
% Time interval
ts = [0, 10]; % [s]
% Initial position
x0 = 0; % [m]
% Initial velocity
dx0 = 0;   % [m/s]
% Initial state vector
X0 = [dx0; x0];
% Excitation force
tin = (ts(1): 0.01 : ts(end))';
uin = min(1, max(tin - 2, 0));
% Solve ode
[t, X] = ode45(@(t, X) mds(t, X, tin, uin), ts, X0);
% Components
dx = X(:, 1);
x = X(:, 2);
% Plot results
figure('Name', 'ode_mds', 'Unit', 'Centimeters', 'Position', [0, 0, 16, 16], 'Color', [1, 1, 1]);
subplot(3,1,1);
plot(t, dx, 'r-');
title('Velocity',...
      'Interpreter', 'Latex',...
      'HorizontalAlignment', 'Center');
xlabel('$t$', 'Interpreter', 'Latex');
ylabel('$dx$', 'Interpreter', 'Latex');
set(gca, 'TickLabelInterpreter', 'Latex', 'FontSize', 10);
grid on;
box on;

subplot(3,1,2);
plot(t, x, 'r-');
title('Position',...
      'Interpreter', 'Latex',...
      'HorizontalAlignment', 'Center');
xlabel('$t$', 'Interpreter', 'Latex');
ylabel('$x$', 'Interpreter', 'Latex');
set(gca, 'TickLabelInterpreter', 'Latex', 'FontSize', 10);
grid on;
box on;

subplot(3,1,3);
plot(tin, uin, 'r-');
title('Input force',...
      'Interpreter', 'Latex',...
      'HorizontalAlignment', 'Center');
xlabel('$t$', 'Interpreter', 'Latex');
ylabel('$u$', 'Interpreter', 'Latex');
set(gca, 'TickLabelInterpreter', 'Latex', 'FontSize', 10);
grid on;
box on;

%% Simple mass-damper-spring system
function dX = mds(t, X, tin, uin)
m = 1;  % [kg]   mass
d = 2;  % [Ns/m] damping coefficient
s = 20; % [N/m]  spring coefficient
A = [-d/m, -s/m;
        1,    0];
B = [1/m; 
       0];
u = interp1q(tin, uin, t);
dX = A * X + B * u;
end
% M�trix el��ll�t�sa (n x n) m�retben, melynek f��tl�j�ban az 1!, 2!, ..., n! faktori�lis �rt�kek
% tal�lhat�ak.
function A = factdiag(n)
A = diag(factorial(1:n));
end
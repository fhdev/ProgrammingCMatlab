% K�z�s elemek sz�m�nak megtal�l�sa k�t m�trixban.
function z = common(x, y)
n = min(size(x), size(y));
z = sum(sum(x(1:n(1), 1:n(2)) == y(1:n(1), 1:n(2))));
end
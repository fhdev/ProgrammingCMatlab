clear;
close all;
% Parameterek
R1 = 1e3;  % [Ohm]
R2 = 5e3;  % [Ohm]
C  = 2e-5; % [Farad]
% Sz�m�t�s ideje �s kezdeti �rt�k
ts = [0; 0.5];
X0 = [0; 0];
% Be�ll�t�sok
o = odeset('MaxStep', 1e-5);
% Mozg�segyenlet megold�sa
[t, y] = ode45(@(t, y)rendszer(t, y, R1, R2, C), ts, X0, o);
% Ellen�rz�s
% Ir�ny�t�stechnika 2 feladatgy�jtem�ny 1.2.6 mechanikai rendszer 
sys = tf([R1 * R2 * C, R2], [R1 * R2 * C, R1 + R2]);
[y2, t2] = step(sys, 0.5);
% Eredm�nyek megjelen�t�se
figure;
hold on;
plot(t2, y2, 'r:', 'LineWidth', 2);
plot(t, y, 'b-');
title('output');
xlabel('t');
ylabel('y');
legend('step', 'ode');
%% Ir�ny�t�stechnika 2 feladatgy�jtem�ny 1.2.5 elektronikai rendszer 
function dy = rendszer(t, y, R1, R2, C)
% Bemenet
tuf = 1e-3;
u = min(1 / tuf * t, 1);
du = 1 / tuf * (t < tuf);
% Deriv�lt sz�m�t�sa
dy = du + 1 / (R1 * C) * u - (R1 + R2) / (R1 * R2 * C) * y;
end
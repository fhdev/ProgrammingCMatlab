% Unconstrained optimization of Rosenbrock function with fminunc
%% Clear workspace
clear;
close all;
clc;
%% Rosenbrock function
rsb = @(a, b, x, y) (a - x).^2 + b * (y - x.^2).^2;
rsbp = @(x, y) rsb(1, 100, x, y);
%% Evaluate function on a grid
% Resolution of grid
r = 0.5;
% Limits of grid.
lim = 10;
% Create grid to evaluate on.
[x, y] = meshgrid(-lim:r:lim,-lim:r:5 * lim);
% Evaluate function on grid.
z = rsbp(x, y);
%% Perform unconstrained optimization from different starting points
% Optimalization options.
opts = optimoptions('fminunc',...
                    'Algorithm', 'quasi-newton',...
                    'Display', 'iter',...
                    'OptimalityTolerance', 1e-6);
% Create all possible two element combination from -lim, lim.
[x0, y0] = ndgrid([-lim, lim], [-lim, lim]);
n = numel(x0);
x0 = x0(:);
y0 = y0(:);
% Preallocate vectors for storing results
z0 = zeros(n, 1);
xm = z0;
ym = z0;
zm = z0;
% Function handle that replaces the two arguments (x,y) with one vector argument (X)
f = @(X) rsbp(X(1), X(2));
for i = 1 : n
    % Evaluate function at initial point.
    z0(i) = rsbp(x0(i), y0(i));
    % Run optimization.    
    [Xmin, zmin] = fminunc(f, [x0(i), y0(i)], opts);
    % Put result to corresponding vectors.
    xm(i) = Xmin(1);
    ym(i) = Xmin(2);
    zm(i) = zmin;
    % Print results.
    fprintf('Minimum is at (x,y)=(%.3f,%.3f), f=%.3f.\n', xm(i), ym(i), zmin);
end
%% Plot results
% Plot results of unconstrained optimization of Rosenbrock function with fminunc
red     = [1, 0, 0];
green   = [0, 1, 0];
blue    = [0, 0, 1];
white   = [1, 1, 1];
figure('Name', 'minunc',...
       'Unit', 'Centimeters',...
       'Position', [0, 0, 14, 10],...
       'Color', [1, 1, 1]);
hold on;
% Plot surface of Rosenbrock function.
surf(x, y, z, 'EdgeColor', 'Interp', 'FaceColor', 'Interp');
title('Unconstrained minimization of the Rosenbrock function', 'Interpreter', 'Latex');
% Plot initial points.
scatter3(x0, y0, z0, 'MarkerEdgeColor', blue, 'MarkerFaceColor', blue);
% Plot found minimum points.
scatter3(xm, ym, zm, 'MarkerEdgeColor', green, 'MarkerFaceColor', green);
% Plot actual minimum.
scatter3(1, 1, 0, 'MarkerEdgeColor', red, 'MarkerFaceColor', red);
% Link the initial points with the minimas that were found starting from them.
plot3([x0'; xm'], [y0'; ym'], [z0'; zm'], 'Color', white);
xlabel('x', 'Interpreter', 'Latex');
ylabel('y', 'Interpreter', 'Latex');
zlabel('z', 'Interpreter', 'Latex');
set(gca, 'TickLabelInterpreter', 'Latex', 'FontSize', 12);
grid on;
box on;
view([-20, 47]);
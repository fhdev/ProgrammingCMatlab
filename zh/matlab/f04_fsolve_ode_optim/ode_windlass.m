clear;
close all;
% Rarameters
g = 9.81;
m = 1;
s = 45;
d = 2;
R = 0.1;
theta = 1;
% Bemenet
M = 0.2;
% Sz�m�t�s ideje �s kezdeti �rt�k
ts = [0; 10];
X0 = [0; -m * g / s; 0; 0];
% Be�ll�t�sok
o = odeset('MaxStep', 1e-2);
% Mozg�segyenlet megold�sa
[t, X] = ode45(@(t, X)rendszer(t, X, g, m, s, d, R, theta, M), ts, X0, o);
% Eredm�nyek megjelen�t�se
figure;

subplot(2,2,1);
plot(t, X(:, 1), 'b-');
xlabel('t [s]');
ylabel('dx [m/s]');
box on; 

subplot(2,2,3);
plot(t, X(:, 2), 'b-');
xlabel('t [s]');
ylabel('x [m]');
box on; 

subplot(2,2,2);
plot(t, rad2deg(X(:, 3)), 'b-');
xlabel('t [s]');
ylabel('dphi [�/s]');
box on; 

subplot(2,2,4);
plot(t, rad2deg(X(:, 4)), 'b-');
xlabel('t [s]');
ylabel('phi [�]');
box on; 
%% Hosszir�ny� j�rm�dinamika
function dX = rendszer(t, X, g, m, s, d, R, theta, M)

% input
M = m * g * R + M * (double(t > 2) - double(t > 8));

% derivatives
F = (R * X(4) - X(2)) * s + (R * X(3) - X(1)) * d;
dX = zeros(4, 1);
dX(1) = 1 / m * (F - m * g);
dX(2) = X(1);
dX(3) = 1 / theta * (M - F * R);
dX(4) = X(3);

end
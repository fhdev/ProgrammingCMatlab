% Simulate the motion of a mass-damper-spring system in Simulink
%% Clean workspace and windows
clear;
close all;
clc;
%% Load parameters and input
mds_param;
mds_input;
%% Simulate model
mdl = 'mds_mdl_mask';
% Load model if necessary
if ~bdIsLoaded(mdl)
    open_system(mdl);
end
% Set stop time and step size
set_param(mdl, 'StopTime', num2str(t_in(end)));
set_param(mdl, 'FixedStep', num2str(t_step));
tic;
% Start simulation
sim(mdl);
toc;
%% Plot results
plot_ode;

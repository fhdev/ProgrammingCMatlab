function binary = decimal2binary(decimal)
if decimal == 0
    len = 1;
else
    len = floor(log2(decimal)) + 1;
end
binary = zeros(1, len);
while decimal > 0
    binary(len) = mod(decimal, 2);
    decimal = floor(decimal / 2);
    len = len - 1;
end
end
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>

#define N 10
#define MAX 10.0
#define MIN -10.0

int general(double x[], int n, double min, double max)
{
	for (int i = 0; i < n; i++)
	{
		x[i] = ((double)rand() / (double)RAND_MAX) * (max - min) + min;
	}
	return n;
}

int movsum(double x[], int n, int m, double y[])
{
	for (int i = 0; i < n; i++)
	{
		y[i] = 0.0;
		int l = i - m / 2;
		for (int j = l; j < l + m; j++)
		{
			if ((j < 0) || (j > n - 1))
				continue;
			y[i] += x[j];
		}
	}
	return n;
}

int movmean(double x[], int n, int m, double y[])
{
	movsum(x, n, m, y);
	for (int i = 0; i < n; i++)
	{
		int l = i - m / 2;
		int r = l + m - 1;
		int d = (r > (n - 1) ? (n - 1) : r) - (l < 0 ? 0 : l) + 1;
		y[i] = y[i] / d;
	}
	return n;
}

void kiir(double x[], int n, int m)
{
	for (int i = 0; i < n; i++)
	{
		printf("%g ", x[i]);
		if (((i + 1) % m) == 0)
			printf("\n");
	}
}


int main(int argc, char *argv[])
{
	srand(time(0));
	printf("Kerem adja meg az ablak meretet! m = ");
	int m;
	scanf("%d", &m);
	double x[N];
	double y[N];
	general(x, N, MIN, MAX);
	kiir(x, N, 10);
	movsum(x, N, m, y);
	kiir(y, N, 10);
	movmean(x, N, m, y);
	kiir(y, N, 10);
}

% This function describes the equations of motion of a mass-spring-damper system.
% ddx = -(d/m).dx - (s/m).x + (1/m).u
%  dx =      1.dx +     0.x +     0.u
function dX = mds_fcn(t, X, t_in, u_in)
    % Parameter values can be accessed via global variables
    global m d s;
    % Compose system matrix
    A = [-d/m, -s/m;
            1,    0];
    % Compose input matrix
    B = [ 1/m;
            0];
    % Interpolate current input value based on the provided time and input data
    u = interp1(t_in, u_in, t);
    % Calculate state derivative
    dX = A * X + B * u;
end
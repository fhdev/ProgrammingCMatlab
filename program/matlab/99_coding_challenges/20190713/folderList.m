% Create a MATLAB function that lists the content of a folder to a tree.
% The first argument of the function should be the parent folder. If no folder is provided, the
% function should use the current folder.
% 1.) The content of the parent folder shall be listed in the format below. (1 point)
%     |- [sub1]
%     |   |-[subsub1]
%     |   |   '- file1.txt
%     |   |_ file2.txt
%     |- [sub2]
%     |   |- file3.txt
%     |   '- file4.txt
%     '- file5.txt
% 2.) Content shall be ordered by name, and folders shall come before files. (1 point)
% 3.) The function shall be able to handle any folder depth. (1 point)
% 4.) The function shall be robust and give meaningful error messages of type 
%     'MATLAB:InvalidArgument' when invalid argument(s) are used. (1 point)
% 
function folderList(folder, indent)
%% Constants
UP_AND_RIGHT = char(hex2dec('2514'));
VERTICAL_AND_RIGHT = char(hex2dec('251c'));
VERTICAL = char(hex2dec('2502'));
%% Default arguments
if nargin < 2
    if nargin < 1
        folder = cd();
    end
    indent = '';
end
%% Check arguments
if ~ischar(folder) || ~isvector(folder) || (size(folder, 1) ~= 1)
    error('MATLAB:InvalidArgument', 'ERROR: folder should be a row vector of chars!');
end
if (nargin > 1) && (~ischar(indent) || ~isvector(indent) || (size(indent, 1) ~= 1))
    error('MATLAB:InvalidArgument', 'ERROR: indent should be a row vector of chars!');
end
%% Print folder list
% Get content as structure
children = dir(folder);
% Get content names
nameChildren = {children.name};
% Get logical indices of self and parent folder
selfAndParentIndex = cellfun(@(c)any(strcmp(c, {'.', '..'})), nameChildren, 'UniformOutput', false);
selfAndParentIndex = [selfAndParentIndex{:}];
% Get logical indices of directories
isdirChildren = [children.isdir];
% Cut out self and parent folder
isdirChildren = isdirChildren(~selfAndParentIndex);
nameChildren = nameChildren(~selfAndParentIndex);
% Select directories first and files after
nameChildren = [nameChildren(isdirChildren), nameChildren(~isdirChildren)];
isdirChildren = [isdirChildren(isdirChildren), isdirChildren(~isdirChildren)];
% Process children
numChildren = length(nameChildren);
for iChild = 1 : numChildren
    % Assemble current and new indentation character
    if iChild == numChildren
        % For last child, there is no indentation character for the next level
        actIndent = [indent, '  ', UP_AND_RIGHT, ' '];
        newIndent = [indent, '    '];
    else
        % For intermediate child, there is indentation character for the next level
        actIndent = [indent, '  ', VERTICAL_AND_RIGHT, ' '];
        newIndent = [indent, '  ', VERTICAL, ' '];
    end
    if isdirChildren(iChild)
        % Process subfolders
        fprintf('%s%s\n', actIndent, ['[', nameChildren{iChild}, ']']);
        subFolder = fullfile(folder, nameChildren{iChild});
        folderList(subFolder, newIndent);
    else
        % Print element
        fprintf('%s%s\n', actIndent, nameChildren{iChild});
    end
end
end
% Random m�trix gener�l�sa (n x n) m�retben, melynek minden sor�ban �s oszlop�ban pontosan 1 db.
% elem 1, a t�bbi elem pedig 0.
function A = snake(n)
A = eye(n);
A = A(randperm(n),:);
end
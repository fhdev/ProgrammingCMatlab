% Function that evaluates another function passed by the function handle parameter fcn, at x.
function y = p_eval_fcn(fcn, x)
    y = fcn(x);
end
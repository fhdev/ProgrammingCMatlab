clear;
close all;
% F�ggv�ny�rt�kek a megjelen�t�shez
d = -5:0.1:5;
[x, y] = meshgrid(d, d);
z = beale(x, y);
% Kezdeti becsl�s
X0 = [0, 3;
      0, 1];
% Optimiz�land� f�ggv�ny
f = @(X)beale(X(1), X(2));
% Be�ll�t�sok
o = optimoptions('fmincon', 'StepTolerance', 1e-3, 'FunctionTolerance', 1e-3, 'Display', 'none');
% Optimaliz�l�s futtat�sa
nX0 = size(X0, 2);
Xmin = zeros(2, nX0);
zmin = zeros(1, nX0);
for i = 1 : nX0
    [Xmin(:, i), zmin(i)] = fmincon(f, X0(:,i), [], [], [], [], [4; -2], [5; 0], [], o);
    fprintf('Minimum is at: %s. Function value is: %s.\n', mat2str(Xmin(:,i)), mat2str(zmin(i)));
end
% Eredm�nyek megjelen�t�se
figure;
hold on;
surf(x, y, z);
scatter3(Xmin(:,1), Xmin(:,2), zmin, 'MarkerEdgeColor', 'red', 'MarkerFaceColor', 'red');
scatter3(3, 0.5, 0, 'MarkerEdgeColor', 'green', 'MarkerFaceColor', 'green');
xlabel('x');
ylabel('y');
zlabel('z');
title('Minimum of Beale function');

% Beale f�ggv�ny
function z = beale(x, y)
z = (1.5 - x + x .* y).^2 + (2.25 - x + x .* y.^2).^2 + (2.625 - x + x .* y.^3).^2;
end 
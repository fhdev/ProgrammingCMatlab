function  n = gcd_dji(a, b)
% Greatest common divisor, Djikstra algorithm
% gcd(a,b)=   a           if a==b
%             gcd(a-b,b)  if a>b
%             gcd(a,b-a)  if a<b
% Test with 1924,4212->52
disp([a b]);
if a==b
    n=a;
elseif a>b
    n=gcd_dji(a-b,b);
else
    n=gcd_dji(a,b-a);
end
end


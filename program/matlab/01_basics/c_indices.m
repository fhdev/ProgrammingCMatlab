clear;
%% Indexel�s
x = [1, 4, 6, 7]
X = [1, 10, 43, 50;
     2,  5, 70, 10;
     7, 12, 44,  2]
% Indexel�s: 1-t�l n-ig

% Skal�rral
x2 = x(2)

% Els� sor m�sodik oszlop�nak eleme
X12 = X(1, 2)

% Minden sor / oszlop / elem
% M�trix 3. oszlopa
Xx3 = X(:,3)

% Line�ris indexel�s => oszlopfolytonosan
X10 = X(10)

% Minden elem egy oszlopvektorba
Xx = X(:)

% Vektorokkal
% A m�trix 2. �s 3. sora �s 2. �s 3. oszlopa
X2323 = X(2:3, 2:3)

% A m�trix 1. sora, �s a 3. oszlopt�l az utols�ig
X13end = X(1, 3:end)

% P�ros sorok p�ratlan oszlopainak elemei
Xoe = X(2:2:end, 1:2:end)

% Logikai vektorokkal
l = X > 2
X(l)

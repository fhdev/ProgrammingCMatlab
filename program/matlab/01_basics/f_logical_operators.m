clear;
%% Logikai oper�torok
v = randi(100, 1, 10)
w = randi(100, 1, 10)

% Rel�ci�k: <, >, <=, >=, ==, ~=
% Neg�ci�: ~
% Skal�rral
l = v > 50

% Dimenzi�k egyez�se eset�n: elemenk�nti rel�ci�
l2 = v > w

% Logikai f�ggv�nyek: &, |, xor
l_and_l2 = l & l2

l_or_l2 = l | l2

l_xor_l2 = xor(l, l2)

% Skal�r operandusokn�l "r�vidre z�r�s": &&, ||
% Alkalmaz�si p�lda: ha a v(i) > 10, �rjuk ki, hogy alma
i = randi(20);
% c1 = (i < 11) & (v(i) > 10);
c2 = (i < 11) && (v(i) > 10);
if c2
    disp('alma');
end

% Ha a v minden eleme nagyobb mint w adott eleme
l3 = all(v > w)


% Ha a v b�rmely eleme nagyobb mint w adott eleme
l4 = any(v > w)


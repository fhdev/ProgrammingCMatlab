function primek = d2_primkereses(legnagyobb)
primek = zeros(1, floor(legnagyobb / 2));
if legnagyobb < 2
    return
end
primek(1) = 2;
nPrimek = 1;
for szam = 3 : 2 : legnagyobb
	legnagyobbOszto = floor(sqrt(szam));
    prim_e = true;
    for iPrim = 1 : nPrimek
		
        if primek(iPrim) > legnagyobbOszto
            break;
        end
        if mod(szam, primek(iPrim)) == 0
            prim_e = false;
            break;
        end
    end
    if prim_e
        nPrimek = nPrimek + 1;
        primek(nPrimek) = szam;
    end        
end
primek = primek(1 : nPrimek);
end
function  n = gcd_euk(a, b)
% Greatest common divisor, Eucledian algorithm
% gcd(a,0) = a   and   gcd(a,b) = gcd(b, a mod b)           
% Test with 1924,4212->52
disp([a b]);
if b == 0
    n = a;
else
    n = gcd_euk(b, mod(a, b));
end
end
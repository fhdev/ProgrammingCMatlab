% Create a MATLAB function that displays a functioning analog clock. 
% 1.) The clock shall display the current time continuously. (1 point)
% 2.) No memory garbage shall remain after user closes the clock's figure. (1 point)
% 3.) User shall be able to create multiple clocks which are working in parallel. (1 point)
% 4.) MATLAB Command Window shall remain responsive. (1 point)
function classicClock()
%% Start clock
% Create figure for plotting
fig = figure('Color', 'White', 'Unit', 'Normalized');
ax = axes(fig, 'Visible', 'off', 'NextPlot', 'add');
axis(ax, 'equal');
% Disable interactivity in case of newer Matlab versions 
if any(strcmp(version('-release'), {'2018b', '2019a'}))
    disableDefaultInteractivity(ax);
end
% Create timer for clock
clockTimer = timer('Name', 'clockTimer', 'BusyMode', 'drop', ...
    'ExecutionMode', 'FixedRate', 'Period', 0.2, ...
    'TimerFcn', @(timer, event)classicClockTimerFcn(timer, event, ax));
% Start timer
start(clockTimer);
end

function classicClockTimerFcn(timer, ~, ax)
%% Constants
RADIUS = 1;
HOUR_HAND = 0.5;
MINUTE_HAND = 0.7;
SECOND_HAND = 0.9;
%% Calcualte
date = datevec(now);
hourAngle = deg2rad((date(4) + date(5) / 60 + date(6) / 3600) / 12 * 360);
minuteAngle = deg2rad((date(5) + date(6) / 60) / 60 * 360);
secondAngle = deg2rad(date(6) / 60 * 360);
%% Draw
if ~isvalid(ax)
    fprintf('Classic clock window is now closed!\n');
    stop(timer);
    delete(timer);
    return;
end
% Clear previous state
cla(ax);
% Face
t = 0 : 0.01 * pi : 2.1 * pi;
plot(ax, sin(t) + RADIUS, cos(t) + RADIUS, 'Color', 'Black', 'LineWidth', 2);
% Hour hand
plot(ax, [RADIUS, RADIUS + HOUR_HAND * RADIUS * sin(hourAngle)], ...
    [RADIUS, RADIUS + HOUR_HAND * RADIUS * cos(hourAngle)], 'Color', 'Green', 'LineWidth', 3);
% Minute hand
plot(ax, [RADIUS, RADIUS + MINUTE_HAND * RADIUS * sin(minuteAngle)], ...
    [RADIUS, RADIUS + MINUTE_HAND * RADIUS * cos(minuteAngle)], 'Color', 'Red', 'LineWidth', 2);
% Second hand
plot(ax, [RADIUS, RADIUS + SECOND_HAND * RADIUS * sin(secondAngle)], ...
    [RADIUS, RADIUS + SECOND_HAND * RADIUS * cos(secondAngle)], 'Color', 'Blue', 'LineWidth', 1);
drawnow();
end
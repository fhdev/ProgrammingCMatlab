% Finding roots of polynomial
%% Clear workspace
clear;
close all;
clc;
% Specify polynomial 6.x^5 - x^4 + 5.x^3 + 2.x^2 - 10.x + 23 = 0
p = [6, -1, 5, 2, -10, 23];
%% Root calculation
r = roots(p);
disp(r);
%% Create coefficient vector based on roots
p2 = poly(r);
disp(p2);
%% Evaluate polynomial
x = -10 : 0.1 : 10;
y = polyval(p, x);
%% Polynomial multipliction and division 
q = [1, 5, 6];
m = conv(p, q);
disp(m);
[k, r] = deconv(m, p);
disp(k);
disp(r);
%% Derivation and integration
dp = polyder(p);
dy = polyval(dp, x);
ip = polyint(p);
iy = polyval(ip, x);
%% Plot results
figure('Name', 'simple', 'Unit', 'Centimeters', 'Position', [0, 0, 14, 10], 'Color', [1, 1, 1]);
% Plot roots
plot(r, 'o');
title('Roots of $6x^5 - x^4 + 5x^3 + 2x^2 - 10x + 23 = 0$', 'Interpreter', 'Latex');
xlabel('$Re(z)$', 'Interpreter', 'Latex');
ylabel('$Im(z)$', 'Interpreter', 'Latex');
set(gca, 'TickLabelInterpreter', 'Latex', 'FontSize', 10);
box on;
grid on;

figure('Name', 'simple', 'Unit', 'Centimeters', 'Position', [0, 0, 14, 10], 'Color', [1, 1, 1]);
% Plot roots
plot(x, y, 'r-', x, dy, 'b-', x, iy, 'g-');
title('Curve of $6x^5 - x^4 + 5x^3 + 2x^2 - 10x + 23 = 0$', 'Interpreter', 'Latex');
xlabel('x', 'Interpreter', 'Latex');
ylabel('y', 'Interpreter', 'Latex');
set(gca, 'TickLabelInterpreter', 'Latex', 'FontSize', 10);
box on;
grid on;
legend({'original', 'derivative', 'integral'});
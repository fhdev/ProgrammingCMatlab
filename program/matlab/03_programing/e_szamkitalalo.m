% A felez�ses keres�s egy sz�p p�ld�ja a sz�mkital�l� j�t�k.
% A sz�m�t�g�p gondol egy sz�mra, 0-100 k�z�tt, �s a j�t�kos kital�lja
% tippelget�ssel, a sz�m�t�g�p csak annyit mond, hogy a gondolt sz�m 
% kisebb, vagy nagyobb.
function e_szamkitalalo()
gondolt = randi(100);
tippek = 0;
tipp = -1;
while tipp ~= gondolt
    tippek = tippek + 1;
    tipp = input('Tippelj! ');
    if gondolt < tipp
        disp('Az �n sz�mom kisebb!');
    elseif gondolt > tipp
        disp('Az �n sz�mom nagyobb!');
    else
        disp('Gratul�lok, kital�ltad!');
        disp([num2str(tippek), ...
            ' tippre volt sz�ks�ged.']);
    end
end
end


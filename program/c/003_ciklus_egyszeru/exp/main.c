#include <stdio.h>

int main(int argc, char *argv[])
{
    printf("Az e^x ertekenek kozelitese nmax + 1 lepesben.\n");

    printf("Kerem adja meg a lepesek szamat! nmax = ");
    int nmax;
    scanf("%d", &nmax);

    printf("Kerem adja meg x erteket! x = ");
    double x;
    scanf("%lf", &x);

    double fact = 1;
    double pow = 1;
    double exp = 0;
    for(int n = 0; n <= nmax; n++)
	{
		exp += pow / fact;
		// x^n a k�vetkez� k�rre
		pow *= x;
		// n! a k�vetkez� k�rre
		fact *= (n + 1);
	}

	printf("Az eredmeny: e^%.10g = %.10g . \n", x, exp);
    return 0;
}

% Matlab be�p�tett: factorial(x)
function f = c_faktorialis(n)
f = 1;
if n < 0
    disp('Az n �rt�ke csak 0, vagy nagyobb eg�sz sz�m lehet.');
    return;
end
for i = 1 : n
  f = f * i;
end
end
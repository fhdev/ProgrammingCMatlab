#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DARAB         20
#define BITS_IN_BYTE   8

int randbit();
void general(int bajtok[][BITS_IN_BYTE], int nBajtok);
int kiertekel(int bajtok[][BITS_IN_BYTE], int ertekek[], int nBajtok);

int main()
{
    int bajtok[DARAB][BITS_IN_BYTE];
    int ertekek[DARAB];
    int osszeg;
    srand(time(0));
    printf("Veletlen binaris szamok:\n");
    general(bajtok, DARAB);
    printf("A veletlen binaris szamok decimalis ertekei:\n");
    osszeg = kiertekel(bajtok, ertekek, DARAB);
    printf("Az ertekek osszege: %d\n", osszeg);
    getchar();
    return 0;
}

int randbit()
{
    return (rand() > (RAND_MAX / 2));
}

void general(int bajtok[][BITS_IN_BYTE], int nBajtok)
{
    int iBajt;
    for(iBajt = 0; iBajt < nBajtok; iBajt++)
    {
        int iBit;
        for(iBit = 0; iBit < BITS_IN_BYTE; iBit++)
        {
            bajtok[iBajt][iBit] = randbit();
            printf("%d ", bajtok[iBajt][iBit]);
        }
        printf("\n");
    }
}

int kiertekel(int bajtok[][BITS_IN_BYTE], int ertekek[], int nBajtok)
{
    int osszeg = 0;
    int iBajt;
    for(iBajt = 0; iBajt < nBajtok; iBajt++)
    {
        int iBit;
        int helyiertek = 1;
        ertekek[iBajt] = 0;
        for(iBit = (BITS_IN_BYTE - 1); iBit >= 0; iBit--)
        {
            ertekek[iBajt] += bajtok[iBajt][iBit] * helyiertek;
            helyiertek *= 2;
        }
        osszeg += ertekek[iBajt];
        printf("%3d\n", ertekek[iBajt]);
    }
    return osszeg;
}

% H�romsz�g ter�let�nek kisz�m�t�sa a h�rom oldalhossz alapj�n.
function t = triarea(o)
t = 0;
o = sort(o);
if o(3) < sum(o(1:2))
    ango12 = acos((o(1)^2 + o(2)^2 - o(3)^2) / (2 * o(1) * o(2)));
    mo1 = o(2) * sin(ango12);
    t = o(1) * mo1 / 2;
end
end
clear;
close all;
% F�ggv�ny�rt�kek a megjelen�t�shez
d = -2:0.1:2;
[x, y] = meshgrid(d, d);
z = goldstein_prince(x, y);
% Kezdeti becsl�s
X0 = [2; -2];
% Optimiz�land� f�ggv�ny
f = @(X)goldstein_prince(X(1), X(2));
% Be�ll�t�sok
o = optimset('TolX', 1e-18, 'TolFun', 1e-18);
% Optimaliz�l�s futtat�sa
[Xmin, zmin] = fminsearch(f, X0, o);
disp(Xmin);
% Eredm�nyek megjelen�t�se
figure;
hold on;
surf(x, y, z);
scatter3(Xmin(1), Xmin(2), zmin, 'MarkerEdgeColor', 'red', 'MarkerFaceColor', 'red');
scatter3(0, -1, 3, 'MarkerEdgeColor', 'green', 'MarkerFaceColor', 'green');
xlabel('x');
ylabel('y');
zlabel('z');
title('Minimum of Goldstein-Prince function');

% Goldstein-Prince f�ggv�ny
function z = goldstein_prince(x, y)
z = (1 + (x + y + 1).^2 * (19 - 14 .* x + 3 .* x.^2 - 14 .* y + 6 .* x .* y + 3 .* y.^2)) .* ...
    (30 + (2 .* x - 3 .* y) .^2 .* (18 - 32 .* x + 12 .* x.^2 + 48 .* y - 36 .* x .* y + 27 .* y .^2));
end
%% Egyszer� sz�m�t�sok
%% cos(x).e^(sin(x))+1 hat�zozott integr�lja a [0; 4.pi] tartom�nyon �s megjelen�t�s
clear;
close all;
% Sz�m�t�s
x = 0:0.01:4*pi;
y = cos(x) .* exp(sin(x)) + 1;
iy = cumsum(y(1:end-1) .* diff(x));
i = trapz(x, y);
disp(i);
% Megjelen�t�s
figure;
subplot(2,1,1);
plot(x, y, '-r');
xlim([x(1), x(end)]);
ylim([min(y), max(y)]);
xlabel('x');
ylabel('y');
title('cos(x).e^{sin(x)}+1');
grid on;
box on;
subplot(2,1,2);
plot(x(1:end-1), iy, '-b');
xlim([x(1), x(end)]);
ylim([min(iy), max(iy)]);
xlabel('x');
ylabel('y');
title('int cos(x).e^{sin(x)}+1');
grid on;
box on;
%% cos(x).e^(sin(x))+1 deriv�ltja a [0; 4pi] tartom�nyon �s megjelen�t�s
clear;
close all;
% Sz�m�t�s
x = 0:0.01:4*pi;
y = cos(x) .* exp(sin(x)) + 1;
dy = diff(y) ./ diff(x);
% Megjelen�t�s
figure;
subplot(2,1,1);
plot(x, y, '-r')
xlim([x(1), x(end)]);
ylim([min(y), max(y)]);
xlabel('x');
ylabel('y');
title('cos(x).e^{sin(x)}+1');
grid on;
box on;
subplot(2,1,2);
plot(x(1:end-1), dy);
xlim([x(1), x(end)]);
ylim([min(dy), max(dy)]);
xlabel('x');
ylabel('y');
title('d/dx cos(x).e^{sin(x)}+1');
grid on;
box on;
%% Az x^3-10.x^2+2.x+20 f�ggv�nyt a k�t nagyobbik gy�ke k�z�tti tartom�nyon az x-tengely k�r�l 
% megforgatva mekkora a kapott forg�stest t�rfogata
clear;
close all;
p = [1, -10, 2, 20];
r = roots(p);
x = r(2):1e-2:r(1);
y = polyval(p, x);
V = pi * trapz(x, y.^2);


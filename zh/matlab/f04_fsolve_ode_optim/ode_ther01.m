clear;
close all;
% Parameterek
R = 10; % [Ohm]
c = 450; % [J/(kg.K)]
m = 0.019; % [kg]
S = 0.0011; % [m^2]
alpha = 250; % [W/(m^2.K)]
% Sz�m�t�s ideje �s kezdeti �rt�k
ts = [0; 100];
X0 = 0; % [�C]
% Be�ll�t�sok
o = odeset('MaxStep', 1e-2);
% Mozg�segyenlet megold�sa
[t, y] = ode45(@(t, T)rendszer(t, T, R, c, m, S, alpha), ts, X0, o);
% Eredm�nyek megjelen�t�se
figure;
hold on;
plot(t, y, 'b-');
title('output');
xlabel('t');
ylabel('T');
%% Ellen�ll�s meleged�s
% P = i^2.R = c.m.dT + S.a.T
function dT = rendszer(t, T, R, c, m, S, alpha)
% Bemenet
tuf = 1e-1;
i = min(1 / tuf * t, 1);
%di = 1 / tuf * (t < tuf);
% Deriv�lt sz�m�t�sa
dT = (i^2 * R - S * alpha * T) / (c * m);
end
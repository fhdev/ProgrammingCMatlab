clear;
close all;
% Parameterek
m = 500;
r = 0.32;
f = 0.02;
c = 5.35;
% Sz�m�t�s ideje �s kezdeti �rt�k
ts = [0; 20];
v0 = 0; % [db]
% Be�ll�t�sok
o = odeset('MaxStep', 1e-2);
% Mozg�segyenlet megold�sa
[t, v] = ode45(@(t, v)rendszer(t, v, m, r, f, c), ts, v0, o);
% Eredm�nyek megjelen�t�se
figure;
hold on;
plot(t, v, 'b-');
title('output');
xlabel('t [s]');
ylabel('v [m/s]');
box on; 
grid on;
%% Hosszir�ny� j�rm�dinamika
function dv = rendszer(t, v, m, r, f, c)
% Bemenet sz�m�t�sa
% Bemenet
M = 500 * min(t, 1);
% Deriv�lt sz�m�t�sa
dv = 1 / m * (M / r - (f * m * 9.8065 + c) * v);
end
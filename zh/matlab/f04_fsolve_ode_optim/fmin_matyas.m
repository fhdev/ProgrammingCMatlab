clear;
close all;
% F�ggv�ny�rt�kek a megjelen�t�shez
d = -10:0.1:10;
[x, y] = meshgrid(d, d);
z = matyas(x, y);
% Kezdeti becsl�s
X0 = [2; 5];
% Optimiz�land� f�ggv�ny
f = @(X)matyas(X(1), X(2));
% Be�ll�t�sok
o = optimset('TolX', 1e-6, 'TolFun', 1e-6);
% Optimaliz�l�s futtat�sa
[Xmin, zmin] = fminsearch(f, X0, o);
disp(Xmin);
% Eredm�nyek megjelen�t�se
figure;
hold on;
surf(x, y, z);
scatter3(Xmin(1), Xmin(2), zmin, 'MarkerEdgeColor', 'red', 'MarkerFaceColor', 'red');
scatter3(0, 0, 0, 'MarkerEdgeColor', 'green', 'MarkerFaceColor', 'green');
xlabel('x');
ylabel('y');
zlabel('z');
title('Minimum of Matyas function');

% Matyas f�ggv�ny
function z = matyas(x, y)
z = 0.26 * (x.^2 +y.^2) - 0.48 * x .* y;
end

try 
    % K�s�bb beillesztett helytelen k�d
    % almafa(5)
    % Olyan m�velet, mely nem biztos hogy helyesen lefut, de m�sk�ppen nem ellen�rizhet�
    % Pl.: t�r�lni akarunk egy f�jlt, de nincs hozz� jogosults�gunk
    A = ones(randi(3));
    B = eye(randi(5));
    C = A * B;
catch kivetel
    % Ez a kiv�tel nekem nem baj
    if strcmp(kivetel.identifier, 'MATLAB:innerdim')
        disp('Nem siker�lt a szorz�s, de t�l�lem...');
        C = 0;
    % M�sfajta kiv�teleket viszont nem rejtek el.
    else
        throw(kivetel);
    end
end
% Ennek a k�dnak m�g futnia kellene
disp('Tov�bbi nagyon fontos k�d ...');
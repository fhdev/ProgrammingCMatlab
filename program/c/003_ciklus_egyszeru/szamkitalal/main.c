#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[])
{
    printf("Szamkitalalo jatek\n");
    printf("Gondolok egy egesz szamra 0 es 100 kozott!\n");
    srand(time(0));
	int gondolt = rand() % 101;
	int tipp = -1;
	int hanytipp = 0;
	while(tipp != gondolt)
	{
		hanytipp++;
		printf("Tippelj! A tipped: ");
		scanf("%d", &tipp);
		if(tipp > gondolt)
		{
			printf("Az en szamom kisebb!\n");
		}
		else if(gondolt > tipp)
		{
			printf("Az en szamom nagyobb!\n");
		}
	}
	printf("A szamom valoban a %d. Te %d probalkozasbol talaltad ki.", gondolt, hanytipp);
    return 0;
}

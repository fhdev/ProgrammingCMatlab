% M�trix el��ll�t�sa (n x n) m�retben, melynek f��tl�j�ban a n�gyzetsz�mok tal�lhat�ak.
function A = squarediag(n)
A = diag((1:n).^2);
end
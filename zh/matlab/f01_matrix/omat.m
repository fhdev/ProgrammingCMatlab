% M�trix el��ll�t�sa (n x m) m�retben, melyben az 1, 2, ..., (n x m) sz�mtani sorozat tagjai vannak
% oszlopfolytonosan.
function A = omat(n, m)
A = reshape((1 : n * m), n, m);
end
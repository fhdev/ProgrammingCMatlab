% M�trix el��ll�t�sa (n x n) m�retben, melynek mell�k�tl�j�ban az 1, 3, ... 2n-1 �rt�kek tal�lhat�ak.
function A = antidiagodd(n)
A = rot90(diag(2*n-1 : -2 : 1));
end
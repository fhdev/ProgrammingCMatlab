#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define LEN 100

void diff(const double x[], int len, int times, double dx[]);
void cumsum(const double x[], int len, int times, double X[]);
void printv(const double x[], int len);
void multv(double m, int len, double x[]);
void fcn(int len, double x_min, double x_max, double y[]);

int main()
{
    printf("Differentiation and integration of x^3 * sin(x) in interval [a; b].\n");
    printf("Please specify the interval.\n");

    printf("a = ");
    double a;
    scanf("%lf", &a);
    printf("b = ");
    double b;
    scanf("%lf", &b);

    double y[LEN];
    fcn(LEN, a, b, y);
    printf("y = x^3 * sin(x) @ x=[a; b]\n");
    printv(y, LEN);

    double dy[LEN];
    diff(y, LEN, 1, dy);
    multv((LEN - 1) / (b - a), LEN, dy);
    printf("dy = d/dx{x^3 * sin(x)} @ x=[a; b]\n");
    printv(dy, LEN);

    double Y[LEN];
    cumsum(y, LEN, 1, Y);
    multv((b - a) / (LEN - 1), LEN, Y);
    printf("Y = I{x^3 * sin(x)} @ x=[a; b]\n");
    printv(Y, LEN);


    return 0;
}

// Calculate the nth difference of elements.
void diff(const double x[], int len, int times, double dx[])
{
    int i, j;

    for(i = 0; i < len; i++)
        dx[i] = x[i];

    for(j = 0; j < times; j++)
        for(i = 0; i < len - 1; i++)
            dx[i] = dx[i + 1] - dx[i];

    for(i = len - times; i < len; i++)
        dx[i] = 0;
}

// Calculate the nth cumulative sum of elements.
void cumsum(const double x[], int len, int times, double X[])
{
    int i, j;

    for(i = 0; i < len; i++)
        X[i] = x[i];

    for(j = 0; j < times; j++)
        for(i = 1; i < len; i++)
            X[i] = X[i] + X[i - 1];

}

// Multiply vector with scalar.
void multv(double m, int len, double x[])
{
    int i;
    for(i = 0; i < len; i++)
        x[i] = m * x[i];
}

// Print vector.
void printv(const double x[], int len)
{
    int i;
    printf("[ ");
    for(i = 0; i < len; i++)
        printf("%5g ", x[i]);
    printf("]\n");
}

// Evaluate sin(x) * x^3 in len points between x_min and x_max.
void fcn(int len, double x_min, double x_max, double y[])
{
    int i;
    double x;
    for(i = 0; i < len; i++)
    {
        x = x_min + i * (x_max - x_min) / (len - 1);
        y[i] = sin(x) * pow(x, 3);
    }
}

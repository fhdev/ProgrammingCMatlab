function points = excelIndexTest()
points = 0;
%% Test output for A ... Z
try
    assert(strcmp(excelIndex(1, 1), 'A1'), 'MATLAB:assertion:failed', 'Invalid result for excelIndex(1, 1)');
    assert(strcmp(excelIndex(1, 2), 'B1'), 'MATLAB:assertion:failed', 'Invalid result for excelIndex(1, 2)');
    assert(strcmp(excelIndex(1, 13), 'M1'), 'MATLAB:assertion:failed', 'Invalid result for excelIndex(1, 13)');
    assert(strcmp(excelIndex(1, 21), 'U1'), 'MATLAB:assertion:failed', 'Invalid result for excelIndex(1, 21)');
    assert(strcmp(excelIndex(1, 26), 'Z1'), 'MATLAB:assertion:failed', 'Invalid result for excelIndex(1, 26)');
catch ex
    if strcmp(ex.identifier, 'MATLAB:assertion:failed')
        fprintf('%s\n', ex.message);
        return;
    else
        fprintf('Unexpected exception. Rethrowing... \n');
        throw(ex);
    end
end
points = points + 1;

%% Test output for all
try
    assert(strcmp(excelIndex(1, 26), 'Z1'), 'MATLAB:assertion:failed', 'Invalid result for excelIndex(1, 26)');
    assert(strcmp(excelIndex(2, 27), 'AA2'), 'MATLAB:assertion:failed', 'Invalid result for excelIndex(2, 27)')
    assert(strcmp(excelIndex(3, 52), 'AZ3'), 'MATLAB:assertion:failed', 'Invalid result for excelIndex(3, 52)')
    assert(strcmp(excelIndex(4, 53), 'BA4'), 'MATLAB:assertion:failed', 'Invalid result for excelIndex(4, 53)')
    assert(strcmp(excelIndex(5, 78), 'BZ5'), 'MATLAB:assertion:failed', 'Invalid result for excelIndex(5, 78)')
    assert(strcmp(excelIndex(6, 79), 'CA6'), 'MATLAB:assertion:failed', 'Invalid result for excelIndex(6, 79)')
    assert(strcmp(excelIndex(7, 702), 'ZZ7'), 'MATLAB:assertion:failed', 'Invalid result for excelIndex(7, 702)')
    assert(strcmp(excelIndex(8, 703), 'AAA8'), 'MATLAB:assertion:failed', 'Invalid result for excelIndex(8, 703)')
    assert(strcmp(excelIndex(9, 728), 'AAZ9'), 'MATLAB:assertion:failed', 'Invalid result for excelIndex(9, 728)')
    assert(strcmp(excelIndex(10, 729), 'ABA10'), 'MATLAB:assertion:failed', 'Invalid result for excelIndex(10, 729)')
    assert(strcmp(excelIndex(11, 1378), 'AZZ11'), 'MATLAB:assertion:failed', 'Invalid result for excelIndex(11, 1378)')
    assert(strcmp(excelIndex(12, 1379), 'BAA12'), 'MATLAB:assertion:failed', 'Invalid result for excelIndex(12, 1379)')
    assert(strcmp(excelIndex(13, 2054), 'BZZ13'), 'MATLAB:assertion:failed', 'Invalid result for excelIndex(13, 2054)')
    assert(strcmp(excelIndex(14, 2055), 'CAA14'), 'MATLAB:assertion:failed', 'Invalid result for excelIndex(14, 2055)')
    assert(strcmp(excelIndex(15, 6188), 'ICZ15'), 'MATLAB:assertion:failed', 'Invalid result for excelIndex(15, 6188)')
    assert(strcmp(excelIndex(16, 6189), 'IDA16'), 'MATLAB:assertion:failed', 'Invalid result for excelIndex(16, 6189)')
    assert(strcmp(excelIndex(17, 2^14), 'XFD17'), 'MATLAB:assertion:failed', 'Invalid result for excelIndex(17, 2^14)')
catch ex
    if strcmp(ex.identifier, 'MATLAB:assertion:failed')
        fprintf('%s\n', ex.message);
        return;
    else
        fprintf('Unexpected exception. Rethrowing... \n');
        throw(ex);
    end
end
points = points + 2;

%% Test invalid arguments
goodException = false;
try
    excelIndex('alma', 'korte');
catch ex
    goodException = strcmp(ex.identifier, 'MATLAB:InvalidArgument');
end
if ~goodException
    fprintf('Invalid behavior for excelIndex(''alma'', ''korte'')!\n');
    return;
end

goodException = false;
try
    excelIndex([], 2);
catch ex
    goodException = strcmp(ex.identifier, 'MATLAB:InvalidArgument');
end
if ~goodException
    fprintf('Invalid behavior for excelIndex([], 2)!\n');
    return;
end

goodException = false;
try
    excelIndex(1, {32, 35});
catch ex
    goodException = strcmp(ex.identifier, 'MATLAB:InvalidArgument');
end
if ~goodException
    fprintf('Invalid behavior for excelIndex(1, {32, 35})!\n');
    return;
end

goodException = false;
try
    excelIndex(2^20 + 1, 2^14 + 1);
catch ex
    goodException = strcmp(ex.identifier, 'MATLAB:InvalidArgument');
end
if ~goodException
    fprintf('Invalid behavior for excelIndex(2^20 + 1, 2^14 + 1)!\n');
    return;
end
points = points + 1;

%% Test passed
fprintf('%s\n', [mfilename, ' PASSED!']);
end
n = 100;
r = rand(n, 1);
m = mean(r);
disp(['Az �tlag: ', num2str(m), ' .']);

if any(r < 0.01)
    disp('Van 0.01-n�l kisebb elem!');
end

if any(r > 0.99)
    disp('Van 0.99-n�l nagyobb elem!');
else
    disp('Nincs 0.99-n�l nagyobb elem!');
end

if m < 0.45
    disp('Az �tlag kisebb a v�rtn�l.');
elseif m > 0.55 
    disp('Az �tlag nagyobb a v�rtn�l.');
else
    disp('Az �tlag a v�rtnak megfelel�.');
end
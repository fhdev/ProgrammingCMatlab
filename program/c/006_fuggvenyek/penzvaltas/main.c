#include <stdio.h>
#include <stdlib.h>

// Magyar �kezetekhez
#include <locale.h>
#include <windows.h>

int felvalt(int darab, int osszeg, int cimletek[], int felbontas[]);
void kiir(int darab, int osszeg, int maradek, int cimletek[], int felbontas[]);

int main(int argc, char *argv[])
{
    // Magyar �kezetekhez
    setlocale(LC_ALL, "");
    SetConsoleCP(1250);
    SetConsoleOutputCP(1250);

    printf("P�nz�sszeg felv�lt�sa a lehet� legnagyobb forint c�mletekre!\n");

    printf("K�rem adjon meg egy p�nz�sszeget (eg�sz sz�m)!\n");
    int osszeg;
    printf("osszeg = ");
    scanf("%d", &osszeg);

    int cimletek[] = {20000, 10000, 5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5};
    // Vigy�zat: ez csak statikusan foglalt t�mb�kre igaz!
    int darab = sizeof(cimletek)/sizeof(int);
    int felbontas[12];
    int maradek = felvalt(darab, osszeg, cimletek, felbontas);

    kiir(darab, osszeg, maradek, cimletek, felbontas);

    return 0;
}

int felvalt(int darab, int osszeg, int cimletek[], int felbontas[])
{
    for(int iCimlet = 0; iCimlet < darab; iCimlet++)
    {
        felbontas[iCimlet] = osszeg / cimletek[iCimlet];
        osszeg %= cimletek[iCimlet];
    }
    return osszeg;
}

void kiir(int darab, int osszeg, int maradek, int cimletek[], int felbontas[])
{
    printf("%d HUF = \n", osszeg);
    for(int iCimlet = 0; iCimlet < darab; iCimlet++)
    {
        printf("%5d x %5d HUF + \n", felbontas[iCimlet], cimletek[iCimlet]);
    }
    printf("%13d HUF\n", maradek);
}

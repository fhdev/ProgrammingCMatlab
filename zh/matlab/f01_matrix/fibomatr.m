% M�trix el��ll�t�sa (n x m) m�retben, melynek minden oszlop�ban a Fibonacci-sorozat 1, 2, ..., n
% elemei tal�lhat�k.
function A = fibomatr(n, m)
A = repmat(fibonacci(1:n)', 1, m);
end
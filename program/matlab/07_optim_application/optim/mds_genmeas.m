% Generate noisy "measurement" from the response of the mass-spring-damper system.
%% Clear workspace
clear;
close all;
clc;
%% Load parameters and input
% Load default parameters
mds_param;
% Parameter values for nice results
m = 0.1;    % mass [kg]
d = 0.3;    % damping conatant [Ns/m]
s = 1;      % spring constant [N/m]
% Load input
mds_input;
%% Simulate perfect measurement
% Function handle of system.
sysfcn = @(t, X) mds_fcn(t, X, t_in, u_in);
% Options for ODE solution.
odeopts = odeset('MaxStep', t_step, 'InitialStep', t_step);
% Function handle of ODE solution.
odefcn = @() ode45(sysfcn, [t_start, t_stop], X_0, odeopts);
% Evaluate solution with original parameters
[t_out_0, X_out_0] = odefcn();
%% Crete noisy measurement
% Generate random noise.
noise = 0.25 * mean(abs(X_out_0)) .* (2 * rand(size(X_out_0)) - 1);
% Add random noise to simulated result.
X_meas = X_out_0 + noise;
%% Save measurement
% Save
save('meas.mat', 't_out_0', 'X_out_0', 'X_meas');
% Clear results
clear;
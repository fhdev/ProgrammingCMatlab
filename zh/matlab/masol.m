%% Adjustable parameters
% 'st121' or 'st122'
room = 'st121';
% 0 -> copy files in fileList to PCs
% 1 -> copy everything from PCs
direction = 1;
% language
lang = 'C';
% files
fileList = {'teglalap.exe', 'teglalap.pdf', 'vektoraritmetika.pdf', 'vektoraritmetika.exe'};

%% Copy files
% Number of PCs
if isequal(room, 'st121')
    nComputers = 21;
elseif isequal(room, 'st122')
    nComputers = 25;
else
    error('Invalid room!');
end
% Subfolder on PCs
subFolder = [lang, '_ZH_', datestr(now,'yyyymmdd')];
% Do copy
for iComputer = 1 : nComputers
    computerName = sprintf('%s%02d', room, iComputer);
    folder = fullfile(computerName, subFolder);
    remoteBasePath = fullfile('Z:', folder);
    try
        if direction == 0
            %% Copy to PCs
            if ~exist(remoteBasePath, 'dir')
                mkdir(remoteBasePath);
            end
            for j = 1 : length(fileList)
                localPath = fullfile(cd, fileList{j});
                remotePath = fullfile(remoteBasePath, fileList{j}); 
                copyfile(localPath, remotePath, 'f');
            end
        elseif direction == 1
            %% Copy from PCs
            if exist(remoteBasePath, 'dir')
                localPath = fullfile(cd, folder);
                copyfile(remoteBasePath, localPath, 'f');
            else
                fprintf('Path ''%s'' does not exist!\n', remoteBasePath);
            end
        end
    catch
        warning('Error with path ''%s''!', remoteBasePath);
    end
end
% Function handles introduction
%% Create function handles
% Create function handle for a second order polynomial.
poly2 = @ (x, a, b, c) a * x^2 + b * x + c;
% Function handle with fixed a, b, c parameters.
poly2_111 = @(x) poly2(x, 1, 1, 1);
%% Display results
% Call the function handle with free parameters.
disp(poly2(2, 1, 1, 1));
% Call the function handle with fixed parameters.
disp(poly2_111(2));
% Pass the function handle to another function to evaluate.
disp(p_eval_fcn(poly2_111, 2));
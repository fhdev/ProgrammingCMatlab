#include <stdio.h>

typedef struct
{
	double x;
	double y;
} pont_t;

typedef struct
{
	pont_t p;
	double a;
	double b;
} teglalap_t;

pont_t pont_beolvas()
{
	pont_t p;
	printf("Adja meg a pont koordinatait:\n");
	printf("X = ");
	scanf("%lf", &(p.x));
	printf("Y = ");
	scanf("%lf", &(p.y));
	return p;
}

teglalap_t teglalap_beolvas()
{
	teglalap_t n;
	printf("Adja meg a teglalap oldalhosszait:\n");
	printf("A (vizszintes) = ");
	scanf("%lf", &(n.a));
	printf("B (fuggoleges) = ");
	scanf("%lf", &(n.b));
	printf("Adja meg a teglalap bal also sarkat:\n");
	n.p = pont_beolvas();
	return n;
}

pont_t jobbfelso(teglalap_t t)
{
	pont_t p;
	p.x = t.p.x + t.a;
	p.y = t.p.y + t.b;
	return p;
}

pont_t jobbalso(teglalap_t t)
{
	pont_t p;
	p.x = t.p.x + t.a;
	p.y = t.p.y;
	return p;
}

pont_t balfelso(teglalap_t t)
{
	pont_t p;
	p.x = t.p.x;
	p.y = t.p.y + t.b;
	return p;
}

double terulet(teglalap_t t)
{
	return t.a * t.b;
}

double kerulet(teglalap_t t)
{
	return 2 * t.a + 2 * t.b;
}

unsigned char benne(teglalap_t t, pont_t p)
{
	pont_t np = jobbfelso(t);
	return (p.x >= t.p.x) && (p.x <= np.x) && (p.y >= t.p.y) && (p.y <= np.y);
}

unsigned char metszi(teglalap_t t1, teglalap_t t2)
{
	return benne(t1, t2.p) || benne(t1, balfelso(t2)) || benne(t1, jobbalso(t2)) || benne(t1, jobbfelso(t2));
}

int main(int argc, char *argv[])
{
	printf("Adjon meg ket teglalapot!\n");
	teglalap_t t1 = teglalap_beolvas();
	teglalap_t t2 = teglalap_beolvas();
	if (metszi(t1, t2))
		printf("A ket teglalap metszi egymast.\n");
	else
		printf("A ket teglalap nem metszi egymast.\n");
	double T1 = terulet(t1);
	double T2 = terulet(t2);
	if (T1 > T2)
		printf("Az elso teglalap terulete nagyobb.\n");
	else if (T2 > T1)
		printf("A masodik teglalap terulete nagyobb.\n");
	else
		printf("A ket teglalap terulete egyenlo!\n");
	double k1 = kerulet(t1);
	double k2 = kerulet(t2);
	if (k1 > k2)
		printf("Az elso teglalap kerulete nagyobb.\n");
	else if (k2 > k1)
		printf("A masodik teglalap kerulete nagyobb.\n");
	else
		printf("A ket teglalap kerulete egyenlo.\n");

}


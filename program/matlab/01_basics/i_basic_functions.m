clear;
%% Mátrix és vektor 
M = randi(9,4)
v = 1:7
%% Elemek összege
sumv = sum(v)

% Sorok összege
sumM = sum(M)

% Oszlopok összege
sumM2 = sum(M, 2)

% Összes elem összege
sumM3 = sum(M, 'all')
% sumM3 = sum(M(:));

%% Elemek szorzata (ugyanúgy működik)
prodv = prod(v)

prodM = prod(M)

%% Invertálás
Minv = inv(M)

%% Determináms
Mdet = det(M)

%% Rang
Mrank = rank(M)

%% L2 norma
Mnorm = norm(M)

%% Nyom
Mtrace = trace(M)

%% Sajátérték feladat
[V, D] = eig(M)

N = M*V - V*D

%% Mátrix függvények
% Mátrix exponenciális
E = expm(M)

% Mátrix négyzetgyök
SR = sqrtm(M)

M - SR*SR)
% Mátrix logaritmus
L = logm(M);

M - expm(L);
%% Aritmetika
% Kumulált összeg és szorzat
sv = cumsum(v)

pv = cumprod(v)

% Differenciák
dv = diff(v)

dv2 = diff(v, 2)

% Mozgóösszeg
mv = movsum(v, 2)


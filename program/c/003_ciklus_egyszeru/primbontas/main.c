#include <stdio.h>

int main(int arc, char *argv[])
{
    printf("Primtenyezos felbontas.\n");

    printf("Kerem adjon meg egy pozitiv egesz szamot!\nszam = ");
    int szam;
    scanf("%d", &szam);

    int oszto = 2;
    printf("%d = ", szam);
    while(szam > 1)
	{
		// Az == 0 r�sz nem felt�tlen�l sz�ks�ges, de pl. MISRA k�dban igen.
		if((szam % oszto) == 0)
		{
			printf("%d", oszto);
			szam /= oszto;
			if(szam > 1)
			{
				printf(" x ");
			}
		}
		else
		{
			oszto++;
		}
	}

    return 0;
}

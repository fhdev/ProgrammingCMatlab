#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void feltolt(int *tomb, int darab);
int kiir_szoveg(int *tomb, int darab, char *fajlNev);
void konzolra_szoveg(int* tomb, int darab);

int main(int argc, char *argv[])
{
    printf("Veletlen szamok generalasa fajlba.\n");
    printf("Kerem adja meg hany szamot szeretne: ");
    int n;
    scanf("%d", &n);
    int *szamok = (int*)malloc(n * sizeof(int));
    if (szamok == NULL)
    {
        printf("Memoria foglalas sikertelen!\n");
        return 1;
    }
    feltolt(szamok, n);
    kiir_szoveg(szamok, n, "szamok.txt");
    free(szamok);
    beolvas_szoveg(&szamok, &n, "szamok.txt");
    konzolra_szoveg(szamok, n);
    free(szamok);
    return 0;
}

void feltolt(int *tomb, int darab)
{
    srand(time(0));
    rand();

    for(int i = 0; i < darab; i++)
    {
        *(tomb + i) = rand();
    }
}

int kiir_szoveg(int *tomb, int darab, char *fajlNev)
{
    FILE *fajl = fopen(fajlNev, "w");
    if (fajl == NULL)
    {
        printf("Fajlba iras sikertelen!\n");
        return 1;
    }
    for(int i = 0; i < darab; i++)
    {
        fprintf(fajl, "%d\n", tomb[i]);
    }
    fclose(fajl);
    return 0;
}

int beolvas_szoveg(int **tomb, int *darab, char *fajlNev)
{
    FILE *fajl = fopen(fajlNev, "r");
    if (fajl == NULL)
    {
        printf("Fajl olvasas sikertelen!\n");
        return 1;
    }
    *darab = 10;
    *tomb = malloc(*darab * sizeof(int));
    int i = 0;
    while(fscanf(fajl, "%d", *tomb + i) != EOF)
    {
        i++;
        if(i >= *darab)
        {
            *darab *= 2;
            *tomb = realloc(*tomb, *darab * sizeof(int));
        }
    }
    if(*darab > i)
    {
        *tomb = realloc(*tomb, i * sizeof(int));
        *darab = i;
    }
    return 0;
}

void konzolra_szoveg(int *tomb, int darab)
{
    for(int i = 0; i < darab; i++)
    {
        printf("%d\n", tomb[i]);
    }
}

% Solve nonlinear equation or equation system
%% Clear workspace
clear;
close all;
clc;
%% Magic Formula
mf = @(D, C, B, E, s) ...
    D .* sin(C .* atan(B .* s - E .* (B .* s - atan(B .* s))));
%% Parameters of Pacejka Magic formula tire model
% Friction coefficients [-]
D = 1.0;   
% Shape factors [-]
C = 1.45;   
% Stiffness factors [-]
B = 15.0;   
% Cuvature factors [-]
E = 0.4;
%% Compute slip curve
% Fix parameter values
mfp = @(s) mf(D, C, B, E, s);
% Signed slip values are valid from -1 to 1
s = -1:0.01:1;
% Calculate adhesion coefficients for different values of slip
mu = mfp(s);
%% Solve nonlinear equation to find the slip value where mu is 0.9 * mu_max
mu90 = 0.9 * D;
% Function handle that represents F(x) = 0 that fsolve can handle
F = @(x) mfp(x) - mu90;
% Solve equation from initial point 0
[s90_0, F90_0] = fsolve(F, 0);
% Solve equation from initial point 1
[s90_1, F90_1] = fsolve(F, 1);
% Display result
disp(['Adhesion coefficient reaches the 90% of its maximum at slip value ', num2str(s90_0), ' .']);
disp(['Adhesion coefficient reaches the 90% of its maximum at slip value ', num2str(s90_1), ' .']);
%% Plot results
% Plot results of simple differential equation solution
figure('Name', 'simple', 'Unit', 'Centimeters', 'Position', [0, 0, 14, 10], 'Color', [1, 1, 1]);
hold on;
% Plot slip curve
plot(s, mu);
% Plot points of solution
plot(s90_0, F90_0 + mu90, 'o', 'MarkerEdgeColor', [1 0 0], 'MarkerFaceColor', [1 0 0]);
plot(s90_1, F90_1 + mu90, 'o', 'MarkerEdgeColor', [0 0 1], 'MarkerFaceColor', [0 0 1]);
title('Finding $s$ that $mf(s) = 0.9D$', 'Interpreter', 'Latex');
legend({'$mf(s)$', 'Solution from $s_0=0$', 'Solution from $s_0=1$'},...
        'Interpreter', 'Latex',...
        'FontSize', 10,...
        'Location', 'SouthEast');
xlabel('$s$', 'Interpreter', 'Latex');
ylabel('$\mu$', 'Interpreter', 'Latex');
set(gca, 'TickLabelInterpreter', 'Latex', 'FontSize', 10);
box on;
grid on;
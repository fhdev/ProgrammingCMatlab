% M�trix el��ll�t�sa (n x m) m�retben, melyben az 1, 2, ..., (n x m) sz�mtani sorozat tagjai vannak
% sorfolytonosan.
function A = smat(n, m)
A = reshape((1 : n * m), m, n)';
end
clear;
%% M�retek
X = [1, 10, 43, 50;
     2,  5, 70, 10;
     7, 12, 44,  2]
% M�ret minden dimenzi� ment�n sorvektork�nt
sX = size(X)

% Sorok sz�ma
rX = size(X, 1)

% Oszlopok sz�ma
cX = size(X, 2)

% �sszes elem sz�ma
nX = numel(X)

% A leghosszabb dimenzi� m�rete (vektor hossza)
lX = length(X)

clear;
close all;
% Parameterek
R = 5e3; % [Ohm]
C = 2e-5; % [Farad]
% Sz�m�t�s ideje �s kezdeti �rt�k
ts = [0; 1];
X0 = 0; % [�C]
% Be�ll�t�sok
o = odeset('MaxStep', 1e-2);
% Mozg�segyenlet megold�sa
[t, y] = ode45(@(t, Uc)rendszer(t, Uc, R, C), ts, X0, o);
% Eredm�nyek megjelen�t�se
figure;
hold on;
plot(t, y, 'b-');
title('output');
xlabel('t');
ylabel('T');
%% Kondenz�tor felt�lt�d�s ellen�ll�ssal soros k�rben
% U = R * C * dUc + Uc
function dUc = rendszer(t, Uc, R, C)
% Bemenet
tuf = 1e-3;
U = min(1 / tuf * t, 1);
%di = 1 / tuf * (t < tuf);
% Deriv�lt sz�m�t�sa
dUc = (U - Uc) / (R * C);
end
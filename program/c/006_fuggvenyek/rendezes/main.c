#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Magyar �kezetekhez
#include <locale.h>
#include <windows.h>

#define NOVEKVO 1
#define CSOKKENO -1
#define KIIR 1


void buborekrendezes(int darab, double szamok[], int dir);
void gyorsrendezes(int elso, int utolso, double szamok[], int dir);
void veletlenszamok(int darab, double szamok[]);
void kiir(int darab, double szamok[]);

int main(int argc, char *argv[])
{
    // Magyar �kezetekhez
    setlocale(LC_ALL, "");
    SetConsoleCP(1250);
    SetConsoleOutputCP(1250);

    // Random inicializ�l�s
    srand(time(0));
    rand();

    printf("A burbor�krendez�s �s a gyorsrendez�s �sszehasonl�t�sa!\n");

    printf("K�rem adja meg, h�ny elem� t�mbbel szeretne tesztelni!\n");
    printf("darab = ");
    int darab;
    scanf("%d", &darab);

    double *tomb1 = malloc(darab * sizeof(double));
    double *tomb2 = malloc(darab * sizeof(double));
    veletlenszamok(darab, tomb1);
    memcpy(tomb2, tomb1, darab * sizeof(double));
    kiir(darab, tomb1);

    clock_t start, end;

    start = clock();
    buborekrendezes(darab, tomb1, NOVEKVO);
    end = clock();
    kiir(darab, tomb1);
    printf("Bubor�krendez�s v�letlen t�mb�n: %g sec.\n", (double)(end - start) / CLOCKS_PER_SEC);

    start = clock();
    gyorsrendezes(0, darab-1, tomb2, NOVEKVO);
    end = clock();
    kiir(darab, tomb2);
    printf("Gyorsrendez�s v�letlen t�mb�n: %g sec.\n", (double)(end - start) / CLOCKS_PER_SEC);

    start = clock();
    buborekrendezes(darab, tomb1, CSOKKENO);
    end = clock();
    kiir(darab, tomb1);
    printf("Bubor�krendez�s ellenkez�leg rendezett t�mb�n: %g sec.\n", (double)(end - start) / CLOCKS_PER_SEC);

    start = clock();
    gyorsrendezes(0, darab-1, tomb2, CSOKKENO);
    end = clock();
    kiir(darab, tomb1);
    printf("Gyorsrendez�s ellenkez�leg rendezett t�mb�n: %g sec.\n", (double)(end - start) / CLOCKS_PER_SEC);

    free(tomb1);
    free(tomb2);

    return 0;
}

void veletlenszamok(int darab, double szamok[])
{
    for(int iszam = 0; iszam < darab; iszam++)
    {
        szamok[iszam] = (double)rand();
    }
}

void kiir(int darab, double szamok[])
{
    if(KIIR)
    {
        printf("***************************************************\n");
        for(int iszam = 0; iszam < darab; iszam++)
        {
            printf("%10g\n", szamok[iszam]);
        }
    }
}

void buborekrendezes(int darab, double szamok[], int dir)
{
    dir = dir > 0 ? 1 : -1;
    // Minden l�p�sben a legnagyobb elem a hely�re ker�l a szomsz�dok cser�lget�s�vel.
    for(int iutolso = darab - 1; iutolso >= 0; iutolso--)
    {
        // A m�r sorbarendezett r�szt nem kell �jra �tn�zni.
        for(int iszam = 0; iszam < iutolso; iszam++)
        {
            if(dir * szamok[iszam] > dir * szamok[iszam + 1])
            {
                double seged = szamok[iszam];
                szamok[iszam] = szamok[iszam + 1];
                szamok[iszam + 1] = seged;
            }
        }
    }
}

void gyorsrendezes(int elso, int utolso, double szamok[], int dir)
{
    dir = dir > 0 ? 1 : -1;

    int e = elso, u = utolso;
    double pivot = szamok[(elso + utolso) / 2];

    // Particion�l�s
    while(e <= u)
    {
        while(dir * szamok[e] < dir * pivot)
        {
            e++;
        }
        while(dir * szamok[u] > dir * pivot)
        {
            u--;
        }
        if(e <= u)
        {
            double seged = szamok[e];
            szamok[e] = szamok[u];
            szamok[u] = seged;
            e++;
            u--;
        }
    }

    // Rekurzi�
    if (elso < u)
    {
        gyorsrendezes(elso, u, szamok, dir);
    }
    if(e < utolso)
    {
        gyorsrendezes(e, utolso, szamok, dir);
    }
}

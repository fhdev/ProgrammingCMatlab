close('all');
%% Adatok
t = 0 : 0.01 * pi : 4 * pi;
s1 = sin(t);
s2 = sin(2 * t);
c1 = cos(t);
%% �br�zol�s
szelesseg = 8;
magassag = 8;
betumeret = 8;
fig = figure('Name', 'Szinuszok', 'Color', 'White', 'Unit', 'Centimeters', ...
    'Position', [0, 0, szelesseg, magassag], ...
    'PaperPosition', [0, 0, szelesseg, magassag], ...
    'PaperSize', [szelesseg, magassag]);
ax = axes(fig, 'FontSize', betumeret, 'FontWeight', 'Normal', ...
    'TitleFontWeight', 'Normal', 'TitleFontSizeMultiplier', 1.0, ...
    'LabelFontSizeMultiplier', 1.0, 'Box', 'on');
% egym�s ut�ni plot parancsok egym�ssal egy�tt jelenjenek meg
hold(ax, 'on');
% h�l�
grid(ax, 'on');
% egyes g�rb�k kirajzol�sa
plot3(ax, t, s1, c1, 'Color', [231, 76, 60] / 255, 'LineStyle', '-', 'LineWidth', 1);
plot3(ax, t, s2, c1, 'Color', [76, 231, 60] / 255, 'LineStyle', '-', 'LineWidth', 1);
% feliratok
title(ax, 'Csavarvonalak');
xlabel(ax, 'x [m]');
ylabel(ax, 'y [m]');
zlabel(ax, 'z [m]', 'Rotation', 0); % sz�veg orient�ci�
legend(ax, {'egyszeres', 'ketszeres'}, 'Location', 'Best', ...
    'Orientation', 'Vertical', 'FontSize', betumeret);
% n�zet
view([-50, -20]);

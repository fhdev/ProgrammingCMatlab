#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

// Magyar �kezetek
#include <locale.h>
#include <windows.h>

#define rand_double ((double)rand() / (double) RAND_MAX)
#define RMIN  +50
#define RMAX +100
#define CMIN -100
#define CMAX +100
#define DB 20

typedef struct
{
    double x;
    double y;
} pont_t;

typedef struct
{
    // sug�r
    double r;
    // k�z�ppont
    pont_t k;
} kor_t;

pont_t pont_general(double cmin, double cmax);
double pont_tavolsag(pont_t pont1, pont_t pont2);
void pont_kiir(pont_t pont);
kor_t kor_general(double rmin, double rmax, double cmin, double cmax);
void kor_kiir(kor_t kor);
unsigned char kor_tartalmaz_pont(kor_t kor, pont_t pont);

int main(int argc, char *argv[])
{
    // Magyar �kezetek
    setlocale(LC_ALL, "");
    SetConsoleCP(1250);
    SetConsoleOutputCP(1250);

    // Random inicializ�l�sa
    srand(time(0));
    rand();

    printf("H�ny v�letlen k�r tartalmazza a v�letlen pontot?\n");

    pont_t pont = pont_general(CMIN, CMAX);
    printf("A pont: ");
    pont_kiir(pont);
    printf("\n");

    int szamlalo = 0;
    kor_t korok[DB];
    for(int ikor = 0; ikor < DB; ikor++)
    {
        korok[ikor] = kor_general(RMIN, RMAX, CMIN, CMAX);
        printf("A %3d. k�r: ", ikor + 1);
        kor_kiir(korok[ikor]);
        printf("\n");
        szamlalo += kor_tartalmaz_pont(korok[ikor], pont);
    }
    printf("A pontot %d k�r tartalmazza.\n", szamlalo);

    return 0;
}

pont_t pont_general(double cmin, double cmax)
{
    pont_t pont;
    pont.x = cmin + (cmax - cmin) * rand_double;
    pont.y = cmin + (cmax - cmin) * rand_double;
    return pont;
}

double pont_tavolsag(pont_t pont1, pont_t pont2)
{
    double x = pont1.x - pont2.x;
    double y = pont1.y - pont2.y;
    return sqrt(x * x + y * y);
}

void pont_kiir(pont_t pont)
{
    printf("x = %+10.5lf\ty = %+10.5lf", pont.x, pont.y);
}

kor_t kor_general(double rmin, double rmax, double cmin, double cmax)
{
    kor_t kor;
    kor.r = rmin + (rmax - rmin) * rand_double;
    kor.k = pont_general(cmin, cmax);
    return kor;
}

void kor_kiir(kor_t kor)
{
    printf("r = %+10.5lf\t", kor.r);
    pont_kiir(kor.k);
}

unsigned char kor_tartalmaz_pont(kor_t kor, pont_t pont)
{
    return pont_tavolsag(kor.k, pont) < kor.r;
}

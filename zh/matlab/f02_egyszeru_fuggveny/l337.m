% Sz�vegben megadott bet�k kicser�l�se sz�mokra (leetspeek)
function string = l337(string)
original = 'ABEGIOQSTZ';
replacement = '4836109572';
string = upper(string);
for i = 1 : length(original)
    string(string == original(i)) = replacement(i);
end
end
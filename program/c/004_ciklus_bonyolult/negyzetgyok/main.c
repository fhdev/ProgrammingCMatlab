#include <stdio.h>
#include <math.h>

#define TOL 1e-9
#define ABS(x) ((x)>0?(x):(-(x)))

int main(int argc, char *argv[])
{
    printf("Egy szam negyzetgyokenek megtalalasa.\n");

    printf("Kerlek adj meg egy pozitiv szamot!\nszam = ");
    double szam;
    scanf("%lf", &szam);

	if(szam < 0.0)
	{
		printf("A megadott szam negativ. Kilepes...\n");
	}
	else
	{
		double also, felso, gyok = 0.0, gyok2 = 0.0;

		// Intervallumos keresés kezdeti határai
		if(szam > 1.0)
		{
			also = 1.0;
			felso = szam;
		}
		else
		{
			also = szam;
			felso = 1.0;
		}

		// Keresés amíg a szám és a talált gyök négyzetének különbsége kisebb mint a szám TOL-szorosa
		while(ABS(szam - gyok2) > (TOL * szam))
		{
			// Aktuális becslés
			gyok = (also + felso) / 2.0;
			// Ez matematikailag nem lehetséges. De a gyakorlatban elérhetjük a lebegőpontos számítás
			// pontosságának határát! Próbáljuk ki enélkül az alábbi számokkal!
			// 684569864198641986541986654655614498657
			// 6.2342342342342342342342351352463465463e-65
			if ((gyok == also) || (gyok == felso))
			{
				break;
			}
			// Intervallumfelezéses keresés
			gyok2 = gyok * gyok;
			if(gyok2 < szam)
			{
				also = gyok;
			}
			else if(gyok2 > szam)
			{
				felso = gyok;
			}
			// A lebegőpontos számítás határain belül teljesen pontos megoldás.
			else
			{
				break;
			}
		}

		printf("Az eredmeny: sqrt(%.10g) = %.10g . \n", szam, gyok);
	}
    return 0;
}

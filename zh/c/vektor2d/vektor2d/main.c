#include <stdio.h>
#include <locale.h>
#include <Windows.h>
#include <math.h>

typedef struct
{
	double x;
	double y;
} vektor2d_t;

vektor2d_t vektor_beolvas(char nev[]);
void vektor_kiir(vektor2d_t vektor);
char muvelet_beolvas();
vektor2d_t osszead(vektor2d_t egyik, vektor2d_t masik);
vektor2d_t kivon(vektor2d_t egyik, vektor2d_t masik);
double skalarszoroz(vektor2d_t egyik, vektor2d_t masik);
double hossz(vektor2d_t vektor);
double kozbezart_szog(vektor2d_t egyik, vektor2d_t masik);

int main(int argc, char *argv[])
{
	setlocale(LC_ALL, "");
	SetConsoleCP(65001);
	SetConsoleOutputCP(65001);
	printf("S�kbeli vektor sz�mol�g�p\n");
	vektor2d_t v1 = vektor_beolvas("vektor1");
	vektor2d_t v2 = vektor_beolvas("vektor2");
	char muvelet = muvelet_beolvas();
	vektor2d_t ve = { 0.0, 0.0 };
	double se = 0.0;
	switch (muvelet)
	{
		case '+':
		{
			ve = osszead(v1, v2);
			printf("Az eredm�ny:\n");
			vektor_kiir(ve);
			break;
		}
		case '-':
		{
			ve = kivon(v1, v2);
			printf("Az eredm�ny:\n");
			vektor_kiir(ve);
			break;
		}
		case 's':
		{
			se = skalarszoroz(v1, v2);
			printf("Az eredm�ny:\n%+g\n", se);
			break;
		}
		case 'k':
		{
			se = kozbezart_szog(v1, v2);
			printf("Az eredm�ny:\n%+g\n", se);
			break;
		}
		default:
		{
			printf("Ilyen m�velet nem l�tezik!\n");
		}
	}
	return 0;
}

vektor2d_t vektor_beolvas(char nev[])
{
	vektor2d_t eredmeny = { 0.0, 0.0 };
	printf("K�rem adja meg a(z) %s 2d vektort!\n", nev);
	printf("v.x = ");
	scanf("%lf", &(eredmeny.x));
	printf("v.y = ");
	scanf("%lf", &(eredmeny.y));
	return eredmeny;
}

void vektor_kiir(vektor2d_t szam)
{
	printf("[%+g; %+g]\n", szam.x, szam.y);
}

char muvelet_beolvas()
{
	printf("K�rem adja meg a m�veletet! (+, -, s, k)\n");
	char muvelet;
	getchar();
	scanf("%c", &muvelet);
	return muvelet;
}

vektor2d_t osszead(vektor2d_t egyik, vektor2d_t masik)
{
	vektor2d_t eredmeny = { 0.0, 0.0 };
	eredmeny.x = egyik.x + masik.x;
	eredmeny.y = egyik.y + masik.y;
	return eredmeny;
}

vektor2d_t kivon(vektor2d_t egyik, vektor2d_t masik)
{
	vektor2d_t eredmeny = { 0.0, 0.0 };
	eredmeny.x = egyik.x - masik.x;
	eredmeny.y = egyik.y - masik.y;
	return eredmeny;
}

double skalarszoroz(vektor2d_t egyik, vektor2d_t masik)
{
	return egyik.x * masik.x + egyik.y * masik.y;
}

double hossz(vektor2d_t vektor)
{
	return sqrt(vektor.x * vektor.x + vektor.y * vektor.y);
}

double kozbezart_szog(vektor2d_t egyik, vektor2d_t masik)
{
	double skalar = skalarszoroz(egyik, masik);
	return acos(skalar / (hossz(egyik) * hossz(masik)));
}
% M�trix el��ll�t�sa (n x n) m�retben, melynek f��tl�j�ban x, mell�k�tl�j�ban y �rt�kek vannak. A
% f�- �s mell�k�tl�ban is szerepl� elem �rt�ke x + y legyen.
function A = diagonal2(n, x, y)
I = eye(n);
A = x * I + y * rot90(I);
end
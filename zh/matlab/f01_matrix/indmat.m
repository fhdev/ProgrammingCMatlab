% M�trix el��ll�t�sa (n x m) m�retben, melynek minden a_ij elem�nek �rt�ke az indexeinek �sszege
% (a_ij = i + j).
function A = indmat(n, m)
A = (1:n)' + (1:m);
end
clear;
close all;
% F�ggv�ny�rt�kek a megjelen�t�shez
d = -3:0.1:4;
[x, y] = meshgrid(d, d);
z = mccormic(x, y);
% Kezdeti becsl�s
X0 = [0; 0];
% Optimiz�land� f�ggv�ny
f = @(X)mccormic(X(1), X(2));
% Be�ll�t�sok
o = optimset('TolX', 1e-6, 'TolFun', 1e-6);
% Optimaliz�l�s futtat�sa
[Xmin, zmin] = fminsearch(f, X0, o);
disp(Xmin);
% Eredm�nyek megjelen�t�se
figure;
hold on;
surf(x, y, z);
scatter3(Xmin(1), Xmin(2), zmin, 'MarkerEdgeColor', 'red', 'MarkerFaceColor', 'red');
scatter3(-0.54719, -1.5479, -1.9133, 'MarkerEdgeColor', 'green', 'MarkerFaceColor', 'green');
xlabel('x');
ylabel('y');
zlabel('z');
title('Minimum of McCormic function');

% Booth f�ggv�ny
function z = mccormic(x, y)
z = sin(x + y) + (x - y).^2 - 1.5 * x + 2.5 * y + 1;
end

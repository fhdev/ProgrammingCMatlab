#include <stdio.h>

#include <stdlib.h>
#include "lista.h"

void lista_kiir_int(lista_t* lista);
void lista_kiir_double(lista_t* lista);

int main()
{
    int i;
    double d;
    lista_t *lista_int, *lista_double;
    uj_lista(&lista_int, 1, sizeof(int));
    uj_lista(&lista_double, 1, sizeof(double));

    for (i = 1; i <= 10; i+=1)
    {
        d = i;
        lista_elem_hozzaad(lista_int, &i);
        lista_elem_hozzaad(lista_double, &d);
    }
    lista_kiir_int(lista_int);
    lista_kiir_double(lista_double);

    lista_elem_torol(lista_int, 2);
    lista_elem_torol(lista_double, 2);
    lista_kiir_int(lista_int);
    lista_kiir_double(lista_double);

    i = -1;
    lista_elem_beszur(lista_int, &i, 5);
    d = -1;
    lista_elem_beszur(lista_double, &d, 5);
    lista_kiir_int(lista_int);
    lista_kiir_double(lista_double);

    torol_lista(&lista_int);
    torol_lista(&lista_double);

    return 0;
}

void lista_kiir_int(lista_t *lista)
{
    if (lista == NULL) return;
    int elem;
    printf("{");
    for (int i = 0; i < lista->darab; i++)
    {
        lista_elem_lekerdez(lista, &elem, i);
        printf("%3d", elem);
        if(i < lista->darab - 1) printf(", ");
    }
    printf("}\n");
}

void lista_kiir_double(lista_t *lista)
{
    if (lista == NULL) return;
    double elem;
    printf("{");
    for (int i = 0; i < lista->darab; i++)
    {
        lista_elem_lekerdez(lista, &elem, i);
        printf("%3g", elem);
        if(i < lista->darab - 1) printf(", ");
    }
    printf("}\n");
}


#ifndef LISTA_H
#define LISTA_H

#include <string.h>

#define LISTA_HIBA -1
#define LISTA_OK 0

typedef struct
{
    void *adat;
    int meret;
    int elem_meret;
    int darab;
} lista_t;

int uj_lista(lista_t **lista, unsigned int meret, unsigned int elem_meret);
int torol_lista(lista_t **lista);

int lista_elem_hozzaad(lista_t *lista, void *elem);
int lista_elem_lekerdez(lista_t *lista, void *elem, unsigned int index);
int lista_elem_torol(lista_t *lista, unsigned int index);
int lista_elem_beszur(lista_t *lista, void *elem, unsigned int index);

#endif // LISTA_H

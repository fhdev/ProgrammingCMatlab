function a = h_fuggveny_atlag(x)
x = x(:);
a = sum(x) / length(x);
end
clear;
close all;
%% Egyszer� f�ggv�nyek
% Egy vektorban az elemek h�ny sz�zal�ka nagyobb az �tlagn�l.
k = greateravg(randn(1, 100000));
disp(k);
% Egy vektorban az elemek h�ny sz�zal�ka esik k�v�l a 6-szoros sz�r�s �ltal megadott tartom�nyon. 
l = outer6sigma(randn(1, 100000));
disp(l);
% exp(x) Taylor-sorral
e = expt(1, 99);
disp(e);
% sin(x) Taylor-sorral
s = sint(pi/4, 99);
disp(s);
% cos(x) Taylor-sorral
c = cost(pi/4, 99);
disp(c);
% Monotonit�s vizsg�lat
m = monotonic(1:10);
disp(m);
% K�z�s elemek megtal�l�sa k�t m�trixban.
c = common(eye(5), eye(3));
disp(c);
% K�l�nb�z� elemek megtal�l�sa k�t m�trixban.
c = different(eye(5), eye(3));
disp(c);
% �tv�lt�s 10-esb�l 2-es sz�mrendszerbe
c = decimal2binary(255);
disp(c);
% �tv�lt�s 2-esb�l 10-es sz�mrendszerbe
c = binary2decimal(c);
disp(c);
% P�ros �s p�ratlan elemek sz�m�nak meghat�roz�sa
[o, e] = countoddeven([1, 2, 3; 5, 7, 10]);
disp(o);
disp(e);
% H�romsz�g ter�lete 3 oldalhossz�s�gb�l
c = triarea([3,4,5]);
disp(c);
% Egy sz�vegben az angol abc bet�inek el�fordul�sainak megsz�mol�sa
c = countletters('apple');
disp(c);
% Egy sz�vegben megadott bet�k kicser�l�se sz�mokra (leetspeek)
c = l337('appletree');
disp(c);


#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h>

#define N 101
#define AM 5
#define TP 5

int komparator(const double be[], double ki[], int n, double le, double fel)
{
	double jel = 0.0;
	for (int i = 0; i < n; i++)
	{
		if (be[i] >= fel) 
			jel = 1.0;
		else if (be[i] <= le)
			jel = 0.0; 
		ki[i] = jel;
	}
	return n;
}

int ido(double t[], int n, double eleje, double vege)
{
	double dt = (vege - eleje) / (n - 1);
	for (int i = 0; i < n; i++)
	{
		t[i] = eleje + i * dt;
	}
	return n;
}

int szinusz(double s[], const double t[], int n, double A, double T)
{
	for (int i = 0; i < n; i++)
	{
		s[i] = A * sin(2 * M_PI / T * t[i]);
	}
}

void kiir(double tomb[], int n)
{
	printf("[");
	for (int i = 0; i < n; i++)
	{
		printf("%+7.3lf", tomb[i]);
		if (i < n - 1) printf(", ");
	}
	printf("]\n");
}

int main()
{
	double t[N], s[N], c[N];
	printf("Kerem adja meg az idotartamot!\nt = ");
	double tmax;
	scanf("%lf", &tmax);
	ido(t, N, 0, tmax);
	szinusz(s, t, N, AM, TP);
	komparator(s, c, N, 1.0 / 3.0 * AM, 2.0 / 3.0 * AM);
	printf("Idopontok:\n");
	kiir(t, N);
	printf("Szinusz ertekek:\n");
	kiir(s, N);
	printf("Komparator ertekek:\n");
	kiir(c, N);
	return 0;
}

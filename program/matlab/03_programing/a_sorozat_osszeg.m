%% Sz�moljuk ki a sz�mok �sszeg�t n �s m k�z�tt!
function sum = a_sorozat_osszeg(n, m)
sum = 0;
if n > m
    disp('Azt szeretn�m k�rni, hogy az els� sz�m ne legyen nagyobb a m�sodikn�l');
    return
end
% Els� logikus megold�s a sz�mok �sszead�sa egy k�ls� v�ltoz�ba
for i = n:m
    sum = sum + i;
end
% Ez az algoritmus j� megold�st adott, de min�l hosszabb a sorozat, ann�l 
% t�bb m�velet sz�ks�ges hozz�, a megold�s kisz�molhat�:
% sum=(n+m)*(m-n+1)/2 k�plettel;
sum = (n + m) * (m - n + 1) / 2;
% Ilyen m�don, a v�grehajtand� utas�t�sok sz�ma nem n�vekszik a sz�mtani
% sorozat m�ret�vel
end

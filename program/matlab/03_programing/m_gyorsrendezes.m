function tomb = m_gyorsrendezes(tomb, elso, utolso)
if nargin < 2
    elso = 1;
end
if nargin < 3
    utolso = length(tomb);
end

e = elso;
u = utolso;
pivot = tomb(floor((elso + utolso) / 2));

%% Particionálás
while e <= u
    while tomb(e) < pivot
        e = e + 1;
    end
    while tomb(u) > pivot
        u = u - 1;
    end
    if e <= u
        seged = tomb(e);
        tomb(e) = tomb(u);
        tomb(u) = seged;
        e = e + 1;
        u = u - 1;
    end
end

%% Rekurzió
if elso < u
    tomb = m_gyorsrendezes(tomb, elso, u);
end
if e < utolso
    tomb = m_gyorsrendezes(tomb, e, utolso);
end
function [ ] = drawhanoi( from,to, fig )
% DRAWHANOI a towers glob�lis v�ltoz�ban t�rolt hanoi j�t�k
% egy korongj�t mozgatja from helyr�l to helyre (1,2,3 lehets�ges �rt�kekkel)
% �s kirajzolja a j�t�k �ll�s�t a fig sz�m� figure-re.
global towers;
colors=[0.9 0.2 0.2;
        0.2 0.9 0.2;
        0.2 0.2 0.9;
        0.9 0.9 0.2;
        0.9 0.2 0.9;
        0.2 0.9 0.9];

gca=figure(fig);
cla(gca);
n=size(towers,1);
axis([0 8 0 n+1]);


%kereses
disc=0;
for i=1:n
    disc=towers(i,from);
    if disc~=0;
    towers(i,from)=0;
    break;
    end;
end
for i=1:n
    if (i==n) && (towers(i,to)==0)
        towers(i,to)=disc;
    break;
    elseif towers(i,to)~=0;
    towers(i-1,to)=disc;
    break;
    end;
end

for j=1:3
 rectangle('Position',[2*j-0.05 0 0.1 n],'FaceColor',[0 0 0])
   for i=1:n
        if towers(i,j)~=0
           w=2*towers(i,j)*(1/n);
           rectangle('Position',[2*j-0.5*w n-i w 0.9],'Curvature',0.2,'FaceColor',colors(mod(towers(i,j),6)+1,:))               
        end
    end
end
pause(1);
end


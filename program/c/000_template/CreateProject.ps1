using namespace System.IO

param (
    [Parameter(Mandatory = $true)]
    [string]$projectName,

    [string]$dstFolder=""
)

$srcFolder = $PSScriptRoot

if ([string]::IsNullOrEmpty($dstFolder))
{
    $dstFolder = [Path]::Combine($srcFolder, $projectName)
}
else 
{
    $dstFolder = [Path]::Combine($dstFolder, $projectName)
}

if (-not (Test-Path $dstFolder))
{
    New-Item -ItemType Directory -Path $dstFolder | Out-Null
}
    
# Copy C file
$cTargetPath = [Path]::Combine($dstFolder, "main.c")
if (-not (Test-Path $cTargetPath))
{
    Copy-Item -Path ([Path]::Combine($srcFolder, "main.c")) -Destination $cTargetPath
}

# Create sln file
$slnTargetPath = [Path]::Combine($dstFolder, "$projectName.sln")
if (-not (Test-Path $slnTargetPath))
{
    $slnContent = Get-Content -Path ([Path]::Combine($srcFolder, "project.sln"))
    $slnContent = $slnContent.Replace("#project", $projectName)
    Set-Content -Path $slnTargetPath -Value $slnContent
}

# Create vcxproj file
$vcxprojTargetPath = [Path]::Combine($dstFolder, "$projectName.vcxproj")
if (-not (Test-Path $vcxprojTargetPath))
{
    $vcxprojContent = Get-Content -Path ([Path]::Combine($srcFolder, "project.vcxproj"))
    $vcxprojContent = $vcxprojContent.Replace("#project", $projectName)
    Set-Content -Path $vcxprojTargetPath -Value $vcxprojContent
}

# Copy vcxproj.filters file
$vcxprojfilterTargetPath = [Path]::Combine($dstFolder, "$projectName.vcxproj.filters")
if (-not (Test-Path $vcxprojfilterTargetPath))
{
    Copy-Item -Path ([Path]::Combine($srcFolder, "project.vcxproj.filters")) -Destination $vcxprojfilterTargetPath
}
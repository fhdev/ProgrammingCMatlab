% Cost function for constrained optimization that penalizes the deviation of the simulated motion
% from a given measurement.
function cost = mds_costfcn_mincon(p, odefcn, t_meas, X_meas)
    % Mark parameter values as global variables
    global m d s;
    % The solver is constrained, no negative parameter values can occur
    % Assign parameter values
    m = p(1);
    d = p(2);
    s = p(3);
    % Solve equations of motion.
    [t_out, X_out] = odefcn();
    % Intepolate the solution to the exact time points of the measurement.
    X_meas_ip = interp1(t_meas, X_meas, t_out);
    % Create error signal as the deviation of the solution from the measurement.
    e_meas = X_out - X_meas_ip;
    % Calculate cost as the dot product of error signal with itself
    cost = sum(trapz(t_out, e_meas.^2));
end
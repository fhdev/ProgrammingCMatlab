% Egy m�trixban az elemek h�ny sz�zal�ka nagyobb az �tlagn�l.
function p = greateravg(v)
v = v(:);
p = sum(v > mean(v)) / length(v);
end
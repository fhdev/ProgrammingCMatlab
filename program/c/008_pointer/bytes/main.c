#include <stdio.h>
#include <stdlib.h>

// Magyar �kezetek
#include<locale.h>
#include<windows.h>

typedef struct
{
    int ev;
    int honap;
    int nap;
} datum_t;

void print_bytes(void* data, int size_in_bytes);

int main(int argc, char *argv[])
{
    // Magyar �kezetek
    setlocale(LC_ALL, "");
    SetConsoleCP(1250u);
    SetConsoleOutputCP(1250u);

    // Program
    printf("B�jtok ki�r�sa...\n");

    int x = 1;
    printf("x = %d = ", x);
    print_bytes(&x, sizeof(int));

    double y = 1;
    printf("y = %f = ", y);
    print_bytes(&y, sizeof(double));

    char s[] = "abcdefgh";
    printf("s = %s = ", s);
    print_bytes(s, sizeof(s));

    datum_t d;
    d.ev = 2018;
    d.honap = 10;
    d.nap = 7;

    printf("d .ev = %d .honap = %d .nap = %d = ", d.ev, d.honap, d.nap);
    print_bytes(&d, sizeof(datum_t));

    return 0;
}

void print_bytes(void *data, int size_in_bytes)
{
    for(int ibyte = 0; ibyte < size_in_bytes; ibyte++)
    {
        printf("%02x ", *((unsigned char*)data + ibyte));
    }
    printf("\n");
}

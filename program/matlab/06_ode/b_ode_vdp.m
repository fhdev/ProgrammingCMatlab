% Solve the Van der Pol ODE system
%% Clear workspace
clear;
close all;
clc;
%% Van der Pol equation
vdp = @(t, Y, mu) [Y(2); mu * (1 - Y(1)^2) * Y(2) - Y(1)];
vdp100 = @(t, Y) vdp(t, Y, 100);
%% Specify initial value problem
tspan = [0, 300];
y0 = [2, 0];
%% Solve initial value problem
tic;
% Solution with ODE45 which is an explicit solver not suitable for stiff equations
[t1, y1] = ode45(vdp100, tspan, y0);
disp(['ODE45 finished in ', num2str(toc, '%f'), ' seconds and calculated ',...
      num2str(length(t1), '%d'), ' time steps.']);
tic;
% Solution with ODE23S which is an implicit solver, suitable for stiff equations
[t2, y2] = ode23s(vdp100, tspan, y0);
disp(['ODE23S finished in ', num2str(toc, '%f'), ' seconds and calculated ',...
      num2str(length(t2), '%d'), ' time steps.']);
%% Plot results
% Plot the solution of Van der Pol ODEs 
figure('Name', 'solve_vdp', 'Unit', 'Centimeters', 'Position', [0, 0, 14, 10], 'Color', [1, 1, 1]);
% Plot solution with ode45 on first plot.
subplot(2, 1, 1);
hold on;
scatter(t1, y1(:,1));
scatter(t1, y1(:,2));
ylim([-3, 3]);
title('Solution of Van der Pole equation with ode45',...
      'Interpreter', 'Latex',...
      'HorizontalAlignment', 'Center');
legend({'$y_1$', '$y_2$'}, 'Interpreter', 'Latex', 'FontSize', 10);
xlabel('$t$', 'Interpreter', 'Latex');
ylabel('$y$', 'Interpreter', 'Latex');
set(gca, 'TickLabelInterpreter', 'Latex', 'FontSize', 10);
grid on;
box on;
% Plot solution with ode45 on second plot.
subplot(2, 1, 2);
hold on;
scatter(t2, y2(:,1));
scatter(t2, y2(:,2));
ylim([-3, 3]);
title('Solution of Van der Pole equation with ode23s',...
      'Interpreter', 'Latex',...
      'HorizontalAlignment', 'Center');
legend({'$y_1$', '$y_2$'}, 'Interpreter', 'Latex', 'FontSize', 10);
xlabel('$t$', 'Interpreter', 'Latex');
ylabel('$y$', 'Interpreter', 'Latex');
set(gca, 'TickLabelInterpreter', 'Latex', 'FontSize', 10);
grid on;
box on;
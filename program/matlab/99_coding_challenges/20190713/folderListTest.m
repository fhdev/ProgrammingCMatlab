function folderListTest()
%% Test function
try
    folderList();
    folderList('.\parent');
    folderList('D:\');
    folderList('alma');
catch ex
    fprintf('Unexpected exception. Rethrowing... \n');
    throw(ex);
end

%% Test invalid arguments
goodException = false;
try
    folderList(12);
catch ex
    goodException = strcmp(ex.identifier, 'MATLAB:InvalidArgument');
end
if ~goodException
    fprintf('Invalid behavior for folderList(12)!\n');
    return;
end

goodException = false;
try
    folderList({11});
catch ex
    goodException = strcmp(ex.identifier, 'MATLAB:InvalidArgument');
end
if ~goodException
    fprintf('Invalid behavior for folderList({11})!\n');
    return;
end

end
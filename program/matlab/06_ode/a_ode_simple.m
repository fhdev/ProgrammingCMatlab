% Solve a very simple differential equations numerically
%% Clear workspace
clear;
close all;
clc;
%% Inputs
% Specify time span
t0 = 0;
tf = 10;
% Specify initial value y(t0)
y0 = 10;
%% Solve dy(t) = t, y(0) = 10
% Function that evaluates dy(t) = t
f1 = @(t, y) t;
% Solve the differential equation   y(t) = t^2/2 + 10
[t1, y1] = ode45(f1, [t0, tf], y0);
%% Solve dy(t) = y(t), y(0) = 10
% Function that evaluates dy(t) = y
f2 = @(t, y) y;
% Solve the differential equation   y(t) = 10 . e^t
[t2, y2] = ode45(f2, [t0, tf], y0);
%% Plot results
% Plot results of simple differential equation solution
figure('Name', 'simple', 'Unit', 'Centimeters', 'Position', [0, 0, 14, 10], 'Color', [1, 1, 1]);
subplot(2, 1, 1)
% Plot solution of dy(t) = t
plot(t1, y1, 'r-', t1, t1.^2 / 2 + 10, 'g:');
title('Integration of $\dot{y}(t) = t$, $y(0) = 10$.', 'Interpreter', 'Latex');
xlabel('$t$', 'Interpreter', 'Latex');
ylabel('$y(t)$', 'Interpreter', 'Latex');
set(gca, 'TickLabelInterpreter', 'Latex', 'FontSize', 10);
grid on;
box on;
subplot(2, 1, 2)
% Plot solution of dy(t) = y(t)
plot(t2, y2, 'r-', t2, 10 * exp(t2), 'g:');
title('Integration of $\dot{y}(t) = y(t)$, $y(0) = 10$.', 'Interpreter', 'Latex');
xlabel('$t$', 'Interpreter', 'Latex');
ylabel('$y(t)$', 'Interpreter', 'Latex');
set(gca, 'TickLabelInterpreter', 'Latex', 'FontSize', 10);
grid on;
box on;

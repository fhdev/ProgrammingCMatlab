#include <stdio.h>

int main(int argc, char *argv[])
{
    char szoveg[100];
    int egesz;
    double tort;

    printf("Kerem adjon meg egy szoveget!\n");
    scanf("%[^\n]s", szoveg);
    printf("A beolvasott szoveg: %s\n", szoveg);

    printf("Kerem adjon meg egy egesz szamot!\n");
    scanf("%d", &egesz);
    printf("A beolvasott egesz: %d\n", egesz);

    printf("Kerem adjon meg egy tort szamot!\n");
    scanf("%lf", &tort);
    printf("A beolvasott tort: %lf\n", tort);

    return 0;
}

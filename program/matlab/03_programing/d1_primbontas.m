% Matlab be�p�tett: factor(x)
function primek = d_primbontas(szam)
%% Argumentumok ellenorzese
if (mod(szam, 1) ~= 0) || (szam < 1)
    error('Csak termeszetes szamokra mukodik!');
end
%% Primkereses
oszto = 2;
primek = [];
while szam > 1
    % Oszt�s marad�ka
    if mod(szam, oszto) == 0
        % oszthat�
        primek = [primek, oszto]; %#ok<AGROW>
        szam = szam / oszto;
    else
        % nem oszthat�
        oszto = oszto + 1;
    end
end
end
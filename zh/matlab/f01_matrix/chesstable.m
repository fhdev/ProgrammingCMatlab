% M�trix gener�l�sa (n x n) m�retben, melynek minden a_ij eleme 1, ha i �s j egyszerre p�ratlan vagy
% p�ros (sakkt�bla m�trix).
function A = chesstable(n)
A = zeros(n, n);
A(1:2:n, 1:2:n) = 1;
A(2:2:n, 2:2:n) = 1;
end
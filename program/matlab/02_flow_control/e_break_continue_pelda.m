%% Break
limit = 0.8;
s = 0;
while 1
    tmp = rand;
    if tmp > limit
        break
    end
    s = s + tmp;
end
%% Continue
for n = 1:50
    if mod(n,7)
        continue
    end
    disp(['Divisible by 7: ' num2str(n)])
end
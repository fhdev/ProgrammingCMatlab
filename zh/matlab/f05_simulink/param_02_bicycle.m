clear;
close all;
%% Param�terek
m = 1500;   % [m]
J = 2000;   % [kg.m^2]
v = 20;     % [m/s]
lf = 1.1;   % [m]
lr = 1.3;   % [m]
Cf = 8.5e4; % [N]
Cr = 8.2e4; % [N]
%% LTI �llapott�r reprezent�ci�
% Rendszerm�trix
a11 = - (2 * Cf + 2 * Cr) / (m * v);
a12 = - (2 * Cf * lf - 2 * Cr * lr) / (m * v) - v; 
a21 = - (2 * Cf * lf - 2 * Cr * lr) / (J * v);
a22 = - (2 * Cf * lf^2 + 2 * Cr * lr^2) / (J * v);
A = [a11, a12; 
     a21, a22];
% Bemeneti m�trix
b11 = 2 * Cf / m;
b21 = 2 * Cf * lf / J;
B = [b11;
     b21];
% Kimeneti m�trix
C = [0, 1];
% El�recsatol�si m�trix
D = 0;

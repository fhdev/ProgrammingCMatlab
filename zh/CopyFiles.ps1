using namespace System.IO

param(
    [string]$workingDir = $PSScriptRoot,
    [string]$room = 'st121',
    [string]$direction = 'fromStudents',
    [string]$language = 'C',
    [string]$remoteDrive = 'Z:\'
)

# Check arguments
if (-not (Test-Path $workingDir))
{
    Throw "Working directory $workingDir does not exist."
}

if ($room -eq 'st121')
{
    $nPCs = 21
}
elseif ($room -eq 'st122')
{
    $nPCs = 25
}
else
{
    Throw "Room must be st121 or st122! Value $room is not valud."
}

if (($direction -ne 'toStudents') -and ($direction -ne 'fromStudents'))
{
    Throw "Direction must be toStudents or fromStudents. Value $direction is not valid."
}

if(-not (Test-Path $remoteDrive))
{
    Throw "Remote drive $remoteDrive does not exits."
}

# Copy files
$workingDir = [Path]::Combine($workingDir, $direction)
if (-not (Test-Path -Path $workingDir))
{
    New-Item -Path $workingDir -ItemType Directory -Force | Out-Null
}

$subDirName = "$language`_ZH_$((Get-Date).ToString("yyyyMMdd"))"

for ($iPc = 1; $iPc -le $nPcs; $iPc++)
{
    $pcName = "$room$("{0:00}" -f $iPc)"
    $remoteDir = [Path]::Combine($remoteDrive, $pcName, $subDirName)
    if($direction -eq 'toStudents')
    {
        if(-not(Test-Path -Path $remoteDir))
        {
            Write-Host "Creating remote directory $remoteDir ..."
            New-Item -Path $remoteDir -ItemType Directory -Force | Out-Null
        }
        Write-Host "Copying content of working directory $workingDir to remote directory $remoteDir ..."
        Copy-Item -Path ([Path]::Combine($workingDir, '*')) -Destination $remoteDir -Recurse -Force
    }
    elseif ($direction -eq 'fromStudents')
    {
        $workingDirPc = [Path]::Combine($workingDir, $pcName)
        if(-not (Test-Path -Path workingDirPc))
        {
            New-Item -Path $workingDirPc -ItemType Directory -Force | Out-Null
        }
        if(Test-Path -Path $remoteDir)
        {
            Write-Host "Copying content of remote directory $remoteDir to working subdirectory $workingDirPc ..."
            Copy-Item -Path ([Path]::Combine($remoteDir, '*')) -Destination $workingDirPc -Recurse -Force
        }
        else
        {
            Write-Host "Remote directory $remoteDir not found."            
        }
    }
    else
    {
        Throw "Invalid direction $direction!"
    }
}
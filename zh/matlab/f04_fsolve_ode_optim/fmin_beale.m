clear;
close all;
% F�ggv�ny�rt�kek a megjelen�t�shez
d = -5:0.1:5;
[x, y] = meshgrid(d, d);
z = beale(x, y);
% Kezdeti becsl�s
X0 = [0; 0];
% Optimiz�land� f�ggv�ny
f = @(X)beale(X(1), X(2));
% Be�ll�t�sok
o = optimset('TolX', 1e-3, 'TolFun', 1e-3);
% Optimaliz�l�s futtat�sa
[Xmin, zmin] = fminsearch(f, X0, o);
disp(Xmin);
% Eredm�nyek megjelen�t�se
figure;
hold on;
surf(x, y, z);
scatter3(Xmin(1), Xmin(2), zmin, 'MarkerEdgeColor', 'red', 'MarkerFaceColor', 'red');
scatter3(3, 0.5, 0, 'MarkerEdgeColor', 'green', 'MarkerFaceColor', 'green');
xlabel('x');
ylabel('y');
zlabel('z');
title('Minimum of Beale function');

% Beale f�ggv�ny
function z = beale(x, y)
z = (1.5 - x + x .* y).^2 + (2.25 - x + x .* y.^2).^2 + (2.625 - x + x .* y.^3).^2;
end
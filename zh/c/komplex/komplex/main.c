#include <stdio.h>
#include <locale.h>
#include <Windows.h>

typedef struct
{
	double re;
	double im;
} komplex_t;

komplex_t komplex_beolvas(char nev[]);
void komlex_kiir(komplex_t szam);
char muvelet_beolvas();
komplex_t osszead(komplex_t egyik, komplex_t masik);
komplex_t kivon(komplex_t egyik, komplex_t masik);
komplex_t szoroz(komplex_t egyik, komplex_t masik);
komplex_t oszt(komplex_t egyik, komplex_t masik);

int main(int argc, char *argv[])
{
	setlocale(LC_ALL, "");
	SetConsoleCP(65001);
	SetConsoleOutputCP(65001);
	printf("Komplex sz�mol�g�p\n");
	komplex_t szam1 = komplex_beolvas("szam1");
	komplex_t szam2 = komplex_beolvas("szam2");
	char muvelet = muvelet_beolvas();
	komplex_t eredmeny = { 0.0, 0.0 };
	switch (muvelet)
	{
		case '+':
		{
			eredmeny = osszead(szam1, szam2);
			break;
		}
		case '-':
		{
			eredmeny = kivon(szam1, szam2);
			break;
		}
		case '*':
		{
			eredmeny = szoroz(szam1, szam2);
			break;
		}
		case '/':
		{
			eredmeny = oszt(szam1, szam2);
			break;
		}
		default:
		{
			printf("Ilyen m�velet nem l�tezik!\n");
		}
	}
	printf("Az eredm�ny:\n");
	komlex_kiir(eredmeny);
	return 0;
}

komplex_t komplex_beolvas(char nev[])
{
	komplex_t eredmeny = { 0.0, 0.0 };
	printf("K�rem adja meg a(z) %s komplex sz�mot!\n", nev);
	printf("Re{x} = ");
	scanf("%lf", &(eredmeny.re));
	printf("Im{x} = ");
	scanf("%lf", &(eredmeny.im));
	return eredmeny;
}

void komlex_kiir(komplex_t szam)
{
	printf("%+g%+gi\n", szam.re, szam.im);
}

char muvelet_beolvas()
{
	printf("K�rem adja meg a m�veletet! (+, -, *, vagy /)\n");
	char muvelet;
	getchar();
	scanf("%c", &muvelet);
	return muvelet;
}

komplex_t osszead(komplex_t egyik, komplex_t masik)
{
	komplex_t eredmeny = { 0.0, 0.0 };
	eredmeny.re = egyik.re + masik.re;
	eredmeny.im = egyik.im + masik.im;
	return eredmeny;
}

komplex_t kivon(komplex_t egyik, komplex_t masik)
{
	komplex_t eredmeny = { 0.0, 0.0 };
	eredmeny.re = egyik.re - masik.re;
	eredmeny.im = egyik.im - masik.im;
	return eredmeny;
}

komplex_t szoroz(komplex_t egyik, komplex_t masik)
{
	komplex_t eredmeny = { 0.0, 0.0 };
	eredmeny.re = egyik.re * masik.re - egyik.im * masik.im;
	eredmeny.im = egyik.im * masik.re + egyik.re * masik.im;
	return eredmeny;
}

komplex_t oszt(komplex_t egyik, komplex_t masik)
{
	komplex_t eredmeny = { 0.0, 0.0 };
	double nevezo = masik.re * masik.re + masik.im * masik.im;
	eredmeny.re = (egyik.re * masik.re + egyik.im * masik.im) / nevezo;
	eredmeny.im = (egyik.im * masik.re - egyik.re * masik.im) / nevezo;
	return eredmeny;
}
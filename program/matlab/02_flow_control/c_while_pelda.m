%% Faktoriális számítás
n = 5;
i = 1;
f = 1;
while i <= n
    f = f * i;
    i = i + 1;
end
disp(f);

%% Szöveg összefűzés
s = '';
while length(s) < 100
    i = input('Írj valamit: ');
    switch i
        case 'elég'
            % Azonnal kilép a ciklusból.
            break;
        case 'ezt ne add hozzá'
            % Azonnal ugrás a következő körre.
            continue;
    end
    s = [s, ' ', i];
end
disp(s);
clear;
%% Vektor v�ltoz�k
% Sorvektor
v = [1, 2, 3]

% Oszlopvektor
w = [4; 5; 6]

% Transzpon�l�s
z = w'

%% Stringek
% A stringek a MATLAB-ban karakter vektorok
a = 'alma'
b = char([97 108 109 97])

%% M�trixok
A = [1, 2, 3;
     4, 5, 6]
A'
% Speci�lis m�trixok
% Egys�gm�trix
% N�gyzetes
I = eye(3)

% 3 sor, 6 oszlop
I2 = eye(3, 6)

% Null�k
N = zeros(3,5)

% Egyesek
O = ones(4)

% V�letlen
R = rand(2,3)

RI = randi(100, 3, 1)

%% Sz�mtani sorozat vektorok
% Sorvektor a 0, 1, ..., 10 elemekkel
s1 = 0:10

% N�vekm�ny megad�sa
s2 = 0:2:16

s3 = 10:-1:5

%% �res v�ltoz�
e = []

%% M�trixok �sszef�z�se
% Megfelel� m�retek eset�n a m�trixok �sszef�zhet�ek
M = [v', w, N]

% Mivel a stringek vektorok, �k is �sszef�zhet�ek
s = 'alma'
t = 'fa'
u = [s, t]

% Random szimmetrikus m�trix el��ll�t�sa (n x n) m�retben a_ij : [0; 1]. 
function A = symmat(n)
A = rand(n);
A = (A + A') / 2;
end
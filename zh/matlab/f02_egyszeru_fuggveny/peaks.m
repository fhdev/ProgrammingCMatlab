function [x, y] = peaks(x)
x = x(:);
n = numel(x);
y = true(n, 1);
y(1) = (n == 1) || (x(1) > x(2));
y(end) = (n == 1) || (x(end-1) < x(end));
y(2:end-1) = (x(1:end-2) < x(2:end-1)) & (x(2:end-1) > x(3:end));
x = x(y);
end
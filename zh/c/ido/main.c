#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MASODPERC_PERC  60
#define PERC_ORA        60
#define ORA_NAP         24

#define DARAB           20

int randomido();
void general(int idopontok[], int darab);
int konvertal(int idopontok[], int darab);

int main()
{
    int idopontok[DARAB];
    int delelottDarab = 0;
    srand(time(0));
    general(idopontok, DARAB);
    delelottDarab = konvertal(idopontok, DARAB);
    printf("Az idopontok kozott delelotti: %d db.\n", delelottDarab);
    getchar();
    return 0;
}

int randomido()
{
    int idopont = 0;
    if((rand() % 2) == 0)
	{
		idopont = (int)(((double)rand() / (double)RAND_MAX) *
				(5 * ORA_NAP * PERC_ORA * MASODPERC_PERC - 1));
	}
	else
    {
    	idopont = 5 * ORA_NAP * PERC_ORA * MASODPERC_PERC;
        idopont += (int)(((double)rand() / (double)RAND_MAX) *
						(2 * ORA_NAP * PERC_ORA * MASODPERC_PERC - 1));
    }
    return idopont;
}

void general(int idopontok[], int darab)
{
    int iIdopont;
    for(iIdopont = 0; iIdopont < darab; iIdopont++)
    {
        idopontok[iIdopont] = randomido();
        printf("TS = [%6d]\n", idopontok[iIdopont]);
    }
}

int konvertal(int idopontok[], int darab)
{
    int idoVektor[4];
    int delelottDarab = 0;
    int iIdopont;
    for(iIdopont = 0; iIdopont < darab; iIdopont++)
    {
    	int idopont = idopontok[iIdopont];
        idoVektor[0] = idopont / (ORA_NAP * MASODPERC_PERC * PERC_ORA);
        idopont %= ORA_NAP * MASODPERC_PERC * PERC_ORA;
        idoVektor[1] = idopont / (MASODPERC_PERC * PERC_ORA);
        idopont %= MASODPERC_PERC * PERC_ORA;
        idoVektor[2] = idopont / PERC_ORA;
        idopont %= PERC_ORA;
        idoVektor[3] = idopont;
        if(idoVektor[1] >= 12)
		{
			delelottDarab++;
		}
        printf("D = [%d], T = [%02d:%02d:%02d]\n", idoVektor[0], idoVektor[1], idoVektor[2], idoVektor[3]);
    }
    return delelottDarab;
}



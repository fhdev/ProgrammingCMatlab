% Solve equations of motion of a mass-damper-spring system
%% Clear workspace
clear;
close all;
clc;
%% Load parameters and input
% Load default parameters
mds_param;
% Parameter values for nice results
m = 0.1;    % mass [kg]
d = 0.3;    % damping conatant [Ns/m]
s = 1;      % spring constant [N/m]
% Load input
mds_input;
%% Solve model
% Function handle of system
sysfcn = @(t, X) mds_fcn(t, X, t_in, u_in);
% Options for ODE solution
odeopts = odeset('MaxStep', t_step, 'InitialStep', t_step);
% Solve equations of motion
tic;
[t_out, y_out] = ode45(sysfcn, [t_start, t_stop], X_0, odeopts);
toc;
%% Plot results
plot_ode;
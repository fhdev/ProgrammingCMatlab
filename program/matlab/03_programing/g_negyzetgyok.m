%% Négyzetgyök keresése intervallum felezéssel
function gyok = g_negyzetgyok(szam)
TOL = 1e-9;
if szam > 1
    also = 1;
    felso = szam;
else
    also = szam;
    felso = 1;
end

gyok2 = 0;
while abs(szam - gyok2) > TOL * szam
    gyok = (also + felso) / 2;
    gyok2 = gyok * gyok;
    if gyok2 == szam
        break
    elseif gyok2 < szam
        also = gyok;
    else
        felso = gyok;
    end
end
 
end
clear;
close all;
%% Param�terek
Za = -1.397;
Zq = 1.0;
Ma = -5.47;
Mq = -3.27;
Zd = -0.124;
Md = -13.2;
u0 = 400; % [m/s]
%% LTI �llapott�r reprezent�ci�
% Rendszerm�trix
% x = [a, q, t, h] (�ll�ssz�g, b�lint�si sz�gsebess�g, b�lint�si sz�g, magass�g)
A = [Za, Zq, 0, 0;
     Ma, Mq, 0, 0
     0,  1,  0, 0;
   -u0,  0, u0, 0];
% Bemeneti m�trix
% u = delta (magass�gi korm�ny sz�ge)
B = [Zd;
     Md;
     0;
     0];
% Kimeneti m�trix
C = [1, 0, 0, 0];
% El�recsatol�si m�trix
D = 0;

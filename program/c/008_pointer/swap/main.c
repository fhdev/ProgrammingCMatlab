#include <stdio.h>
#include <stdlib.h>

// Magyar �kezetek
#include<locale.h>
#include<windows.h>

void swap_normal(int a, int b);
void swap_pointer(int *a, int *b);

int main(int argc, char *argv)
{
    // Magyar �kezetek
    setlocale(LC_ALL, "");
    SetConsoleCP(1250u);
    SetConsoleOutputCP(1250u);

    // Program
    printf("K�t sz�m felcser�l�se!\n");
    int a = 1, b = 5;
    printf("main\n");
    printf("a = %d, b = %d\n", a, b);
    swap_normal(a, b);
    printf("swap_normal\n");
    printf("a = %d, b = %d\n", a, b);
    swap_pointer(&a, &b);
    printf("swap_pointer\n");
    printf("a = %d, b = %d\n", a, b);
    return 0;
}

void swap_normal(int a, int b)
{
    int seged = a;
    a = b;
    b = seged;
}

void swap_pointer(int *a, int *b)
{
    int seged = *a;
    *a = *b;
    *b = seged;
}

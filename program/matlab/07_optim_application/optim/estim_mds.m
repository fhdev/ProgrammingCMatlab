% Estimate the parameters of a mass-damper-spring system based on the shape of the displacement plot
%% Read values from displacement plot
u_ss = 1;            % steady-state input force [N]
x_ss_meas = 1;       % steady-state displacement [m]
ksi_meas = 0.5;      % estimation of damping ratio [-]
T_meas = 2.5;        % setting time [s]
%% Calculate the values of parameters (based on analytical solution of ODE)
s_meas = u_ss / x_ss_meas;
m_meas = s_meas / (1 / (T_meas * sqrt(1 - ksi_meas^2) / (2 * pi)))^2;
d_meas = ksi_meas * 2 * sqrt(s_meas * m_meas);


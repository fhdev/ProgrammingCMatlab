clear;
close all;
% F�ggv�ny�rt�kek a megjelen�t�shez
d = -10:0.1:10;
[x, y] = meshgrid(d, d);
z = booth(x, y);
% Kezdeti becsl�s
X0 = [0; 0];
% Optimiz�land� f�ggv�ny
f = @(X)booth(X(1), X(2));
% Be�ll�t�sok
o = optimset('TolX', 1e-6, 'TolFun', 1e-6);
% Optimaliz�l�s futtat�sa
[Xmin, zmin] = fminsearch(f, X0, o);
disp(Xmin);
% Eredm�nyek megjelen�t�se
figure;
hold on;
surf(x, y, z);
scatter3(Xmin(1), Xmin(2), zmin, 'MarkerEdgeColor', 'red', 'MarkerFaceColor', 'red');
scatter3(1, 3, 0, 'MarkerEdgeColor', 'green', 'MarkerFaceColor', 'green');
xlabel('x');
ylabel('y');
zlabel('z');
title('Minimum of Booth function');

% Booth f�ggv�ny
function z = booth(x, y)
z = (x + 2.*y -7).^2 + (2 .* x + y - 5).^2;
end

clear;
close all;
% Parameterek
aR = 0.4;
bR = 0.001;
cF = 0.001;
dF = 0.9;
% Sz�m�t�s ideje �s kezdeti �rt�k
ts = [0; 100];
X0 = [500;500]; % [db]
% Be�ll�t�sok
o = odeset('MaxStep', 1e-2);
% Mozg�segyenlet megold�sa
[t, X] = ode45(@(t, X)rendszer(t, X, aR, bR, cF, dF), ts, X0, o);
% Eredm�nyek megjelen�t�se
figure;
hold on;
plot(t, X(:, 1), 'b-');
plot(t, X(:, 2), 'r-');
title('output');
legend('rabbits', 'foxes');
xlabel('t');
ylabel('R, F');
box on; 
grid on;
%% R�k�k �s nyulak sz�m�nak alakul�sa
function dX = rendszer(t, X, aR, bR, cF, dF)
% Deriv�lt sz�m�t�sa
dX = [0; 0];
dX(1) = (aR - bR * X(2)) * X(1);
dX(2) = (cF * X(1) - dF) * X(2);
end
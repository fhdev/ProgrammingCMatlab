clear;
close all;
% F�ggv�ny�rt�kek a megjelen�t�shez
d = -5:0.1:5;
[x, y] = meshgrid(d, d);
z = himmelblau(x, y);
% Kezdeti becsl�s
X0 = [0; 0];
% Optimiz�land� f�ggv�ny
f = @(X)himmelblau(X(1), X(2));
% Be�ll�t�sok
o = optimset('TolX', 1e-6, 'TolFun', 1e-6);
% Optimaliz�l�s futtat�sa
[Xmin, zmin] = fminsearch(f, X0, o);
disp(Xmin);
% Eredm�nyek megjelen�t�se
figure;
hold on;
surf(x, y, z);
scatter3(Xmin(1), Xmin(2), zmin, 'MarkerEdgeColor', 'red', 'MarkerFaceColor', 'red');
scatter3(3, 2, 0, 'MarkerEdgeColor', 'green', 'MarkerFaceColor', 'green');
scatter3(-2.805118,  3.131312, 0, 'MarkerEdgeColor', 'green', 'MarkerFaceColor', 'green');
scatter3(-3.779310, -3.283186, 0, 'MarkerEdgeColor', 'green', 'MarkerFaceColor', 'green');
scatter3( 3.584428, -1.848126, 0, 'MarkerEdgeColor', 'green', 'MarkerFaceColor', 'green');
xlabel('x');
ylabel('y');
zlabel('z');
title('Minimum of Himmelblau function');

% Himmelblau f�ggv�ny
function z = himmelblau(x, y)
z = (x.^2 + y - 11).^2 + (x + y.^2 - 7).^2;
end
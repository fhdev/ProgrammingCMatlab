% Egy sz�vegben az angol abc bet�inek el�fordul�sainak sz�mol�sa
function v = countletters(string)
abc = 'abcdefghijklmnopqrstuvwxyz';
string = lower(string(:));
v = sum(string == abc);
end
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define KEK 0
#define ZOLD 1
#define PIROS 2

int sorsol(int kek, int zold, int piros);
void gyakorisag_szamol(int kek, int zold, int piros);


int main(int argc, char *argv[])
{
	srand(time(0));
	for (int i = 0; i < 10; i++) rand();
	printf("Adja meg a golyok szamat:\n");
	int kek = beolvas("kek", 5, 10);
	int zold = beolvas("zold", 7, 13);
	int piros = beolvas("piros", 3, 9);
	gyakorisag_szamol(kek, zold, piros);
	return 0;
}

int beolvas(char *nev, int min, int max)
{
	int ertek;
	unsigned char ervenyes;
	do
	{
		printf("%s = ", nev);
		scanf("%d", &ertek);
		if (!(ervenyes = (ertek >= min) && (ertek <= max)))
		{
			printf("A megadott ertek (%d) nincs a [%d, %d] intervallumban.\n", ertek, min, max);
		}
	} while (!ervenyes);
	return ertek;
}

int sorsol(int kek, int zold, int piros)
{
	double osszes = kek + zold + piros;
	double veletlen = (double)rand() / (double)RAND_MAX;
	if (veletlen <= ((double)kek / osszes))	return KEK;
	else if (veletlen <= ((double)(kek + zold) / osszes)) return ZOLD;
	else return PIROS;
}

void gyakorisag_szamol(int kek, int zold, int piros)
{
	int kekSorsolt = 0;
	int zoldSorsolt = 0;
	int pirosSorsolt = 0;
	while ((kek + zold + piros) > 0)
	{
		int golyo = sorsol(kek, zold, piros);
		switch (golyo)
		{
			case KEK:
			{
				kekSorsolt++; 
				kek--;
				break;
			}
			case ZOLD:
			{
				zoldSorsolt++; 
				zold--;
				break;
			}
			case PIROS:
			{
				pirosSorsolt++; 
				piros--;
				break;
			}
			default:
			{
				break;
			}
		}
		double osszes = kekSorsolt + zoldSorsolt + pirosSorsolt;
		printf("K:%.3f Z:%.3f P:%.3f\n", (double)kekSorsolt / osszes, (double)zoldSorsolt / osszes, (double)pirosSorsolt / osszes);
	}
}
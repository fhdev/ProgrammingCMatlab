% Egy m�trixban az elemek h�ny sz�zal�ka esik k�v�l a 6-szoros sz�r�s �ltal megadott tartom�nyon.
function p = outer6sigma(v)
v = v(:);
m = mean(v);
s = std(v);
p = sum((v > (m + 3 * s)) | (v < (m - 3 * s))) / length(v);
end
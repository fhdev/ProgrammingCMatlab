function tomb = h_buborekrendezes(tomb)
for iutolso = length(tomb) : -1 : 1
    for ielem = 1 : iutolso - 1
        if tomb(ielem) > tomb(ielem + 1)
            seged = tomb(ielem);
            tomb(ielem) = tomb(ielem + 1);
            tomb(ielem + 1) = seged;
        end
    end
end
end
% M�trix gener�l�sa (n x n) m�retben, melynek minden a_ij eleme x, ha i p�ros �s j p�ratlan, vagy ha
% i p�ratlan, �s j p�ros.
function A = tiles(n, x)
A = zeros(n, n);
A(1:2:n, 2:2:n) = x;
A(2:2:n, 1:2:n) = x;
end
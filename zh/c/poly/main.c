#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void polyder(const double p[], int len, double dp[]);
void polyint(const double p[], int len, double P[]);
double polyval(const double p[], int len, double x);
void printp(const double p[], int len);
void scanp(double p[], int len);

int main()
{
    printf("Evaluation, integration and derivation of polynomial.\n");

    printf("Please specify the degree of polynomial: ");
    int len;
    scanf("%d", &len);
    len++;

    printf("Please specify the coefficients in descending order of powers.\n");
    double p[len];
    scanp(p, len);
    printf("The polynomial is:\n");
    printp(p, len);

    printf("Derivative of polynomial is:\n");
    double dp[len];
    polyder(p, len, dp);
    printp(dp, len);

    printf("Integral of polynomial is:\n");
    double P[len];
    polyint(p, len, P);
    printp(P, len);

    printf("Please specify query point: ");
    double x;
    scanf("%lf", &x);
    printf("p(%g) = %g\n", x, polyval(p, len, x));

    return 0;
}

// Calculate coefficients of derivative of polynomial
//  p[0] * x^3 +  p[1] * x^2 +  p[2] * x +  p[3]
// dp[0] * x^3 + dp[1] * x^2 + dp[2] * x + dp[3]
// dp[0] = 0, dp[1] = 3 * p[0], dp[2] = 2 *p[1], dp[3] = 1 * p[2]
void polyder(const double p[], int len, double dp[])
{
    int i;
    dp[0] = 0;
    for(i = 1; i < len; i++)
        dp[i] = p[i - 1] * (len - i);
}

// Calculate coefficients of integral of polynomial
//   0  * x^3 + p[1] * x^2 + p[2] * x + p[3]
// P[0] * x^3 + P[1] * x^2 + P[2] * x + P[3]
// P[0] = p[1] / 3, P[1] = p[2] / 2, P[2] = p[3], P[3] = 0
void polyint(const double p[], int len, double P[])
{
    int i;
    if(p[0] != 0) printf("Warning: integration result does not fit into array.\n");
    for(i = 0; i < len - 1; i++)
        P[i] = p[i + 1] / (len - 1 - i);
    P[len - 1] = 0;
}

// Evaluate polynomial
// y = p[0] * x^3 +  p[1] * x^2 +  p[2] * x +  p[3]
double polyval(const double p[], int len, double x)
{
    int i;
    double y = 0;
    for(i = 0; i < len; i++)
        y += p[i] * pow(x, len - 1 - i);
    return y;
}

// Print polynomial coefficients
void printp(const double p[], int len)
{
    int i;
    for(i = 0; i < len; i++)
    {
        printf("%5gx^%d ", p[i], len - 1 - i);
        if (i < len - 1)
            printf(" + ");
    }

    printf("\n");
}

// Scan polynomial coefficients
void scanp(double p[], int len)
{
    int i;
    for(i = 0; i < len; i++)
    {
        printf("p[%d] (x^%d) = ", i, len - 1 - i);
        scanf("%lf", p + i);
    }
}

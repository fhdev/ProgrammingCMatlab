% Plot the simulated motion of a mass-damper-spring system 
figure('Name', 'ode',...
       'Unit', 'Centimeters',...
       'Position', [0, 0, 14, 10],...
       'Color', [1, 1, 1]);
% Plot input and output.
plot(t_in, u_in, t_out, y_out);
title('Step response of mass-damper-spring system', 'Interpreter', 'Latex');
legend({'$u$','$\dot{x}$','$x$'}, 'Interpreter', 'Latex', 'FontSize', 12);
xlabel('$t$ [s]', 'Interpreter', 'Latex');
xlim([t_start, t_stop]);
ylabel('$u$ [N], $\dot{x}$ [m/s], $x$ [m]', 'Interpreter', 'Latex');
set(gca, 'TickLabelInterpreter', 'Latex', 'FontSize', 12);
grid on;
box on;
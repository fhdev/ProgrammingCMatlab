% M�trix el��ll�t�sa (n x m) m�retben, melynek minden sor�ban az 1, 2, ..., m sz�mtani sorozat
% tal�lhat�.
function A = arprogmatr(n, m)
A = repmat(1:m, n, 1);
end
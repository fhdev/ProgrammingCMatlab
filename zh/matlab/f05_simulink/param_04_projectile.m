clear;
close all;
%% Sz�m�t�s
r = 0.05;   % [m] radius of projectile
rhoS = 7800; % [kg/m^3] density of steel
Cd = 0.47;  % [1] drag coefficient of sphere
rhoA = 1.2; % [kg/m^3] density of air
%% Param�terek
m = 4/3 * r^3 * pi * rhoS; % [kg] mass
c = 1/2 * Cd * rhoA * r^2 * pi; % total drag coefficient
g = 9.8065; % [m/s^2] gravity
%% Kezdeti �rt�kek
v0 = [20; 20]; % starting velocity [m/s]
s0 = [0; 200]; % starting position [m]

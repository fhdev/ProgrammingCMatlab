clear;
close all;
%% Param�terek
R = 1.8;      % [Ohm]
L = 2.6e-3;   % [H]
km = 0.035;   % [N.m/A]
ki = 0.03497; % [V.s/rad]
J = 2.4e-6;   % [kg.m^2]
ks = 1e-6;   % [kg.m^2/s]
%% LTI �llapott�r reprezent�ci�
% Rendszerm�trix
A = [-R/L, -ki/L;
     km/J, -ks/J];
% Bemeneti m�trix
B = [1/L,    0;
       0, -1/J];
% Kimeneti m�trix
C = [0, 1];
% El�recsatol�si m�trix
D = 0;

%% Keress�k meg egy vektor vagy m�trix maximum�t
% A legt�bb feladat nem reduk�lhat� 1 l�p�sre, bizonyos esetekben meg kell vizsg�lni minden elemet
% Matlab be�p�tett: max(x)
function y = b_maximumkereses(x)
y = x(1);
for i = 2 : numel(x)
    if x(i) > y
        y = x(i);
    end
end
end
#include <stdio.h>
#include <math.h>

int main(int argc, char *argv[])
{
    printf("Az a.x^2 + b.x + c = 0 egyenlet megoldasa.\n");

    printf("a = ");
    double a;
    scanf("%lf", &a);

    printf("b = ");
    double b;
    scanf("%lf", &b);

    printf("c = ");
    double c;
    scanf("%lf", &c);

    if (a == 0)
    {
        if (b == 0)
        {
            if (c == 0)
            {
                printf("Azonossag! 0 == 0\n");
            }
            else
            {
                printf("Ellentmondas! %g != 0\n", c);
            }
        }
        else
        {
            printf("Elsofoku egyenlet. Megoldas: x = %g .\n", -c/b);
        }
    }
    else
    {
        double D = b*b - 4*a*c;
        if (D == 0)
        {
            printf("Ketszeres multiplicitasu megoldas.\n");
            printf("Megoldas: %g .\n", -b / (2 * a));
        }
        else if (D > 0)
        {
            double x1 = (-b + sqrt(D)) / (2 * a);
            double x2 = (-b - sqrt(D)) / (2 * a);
            printf("Ket valos megoldas.\n");
            printf("Megoldas: %g es %g .\n", x1, x2);
        }
        else
        {
            double re = -b / (2 * a);
            double im = sqrt(-D) / (2 * a);
            printf("Komplex konjugalt megoldas. \n");
            printf("Megoldas: %g+%gi es %g-%gi .\n", re, im, re, im);
        }
    }
    return 0;
}

#include <stdio.h>
#include <stdlib.h>

// Magyar �kezetekhez
#include <locale.h>
#include <windows.h>

int lnko(int szam1, int szam2);
int lnko_d(int szam1, int szam2);
int lkkt(int szam1, int szam2);

int main(int argc, char *argv[])
{
    // Magyar �kezetekhez
    setlocale(LC_ALL, "");
    SetConsoleCP(1250);
    SetConsoleOutputCP(1250);

    printf("Legnagyobb k�z�s oszt� �s legkisebb k�z�s t�bbsz�r�s meghat�roz�sa.\n");
    printf("K�rem adjon meg k�t pozit�v eg�sz sz�mot!\n");

    printf("szam1 = ");
    int szam1;
    scanf("%d", &szam1);

    printf("szam2 = ");
    int szam2;
    scanf("%d", &szam2);

    int oszto1 = lnko(szam1, szam2);
    int oszto2 = lnko_d(szam1, szam2);
    printf("LNKO(%d,%d) = %d (Euklideszi algoritmus)\n", szam1, szam2, oszto1);
    printf("LNKO(%d,%d) = %d (Dijkstra algoritmus)\n", szam1, szam2, oszto1);

    int tobbszor = lkkt(szam1, szam2);
    printf("LKKT(%d,%d) = %d\n", szam1, szam2, tobbszor);
    return 0;
}

// Euklideszi algoritmus
int lnko(int szam1, int szam2)
{
    while(szam2 > 0)
    {
        int temp = szam2;
        szam2 = szam1 % szam2;
        szam1 = temp;
    }
    return szam1;
}

// Djikstra algoritmus
int lnko_d(int szam1, int szam2)
{
    int lnko = 0;
    if(szam1 == szam2)
    {
        lnko = szam1;
    }
    else if (szam1 > szam2)
    {
        lnko = lnko_d(szam1 - szam2, szam2);
    }
    else
    {
        lnko = lnko_d(szam1, szam2- szam1);
    }
    return lnko;
}

int lkkt(int szam1, int szam2)
{
    return szam1 * szam2 / lnko(szam1, szam2);
}

% M�trix n x m m�retben, melynek elemei az 1, 2, ..., n * m sz�mtani sorozat tagjai, v�letlenszer�en
function A = randpermmat(n, m)
A = zeros(n, m);
A(:) = randperm(n * m);
end
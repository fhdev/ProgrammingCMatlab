% Create a MATLAB function that converts a regular 1 based row and column index pair to Excel cell
% reference. E.g. a cell in the 10th row and 27th column is referred to as AA10. Excel can handle
% 1 048 576 rows and 16 384 columns.
% 1.) The function shall give appropriate result at least for columns A to Z. (1 point)
% 2.) The function shall give appropriate results for any indices. (2 point)
% 3.) The function shall be robust and give meaningful error messages of type 
%     'MATLAB:InvalidArgument' when invalid argument(s) are used. (1 point)
function index = excelIndex(row, col)
%% Constants
BASE = 27;
%% Check argumtens
if ~isnumeric(row) || ~isscalar(row) || (row < 1) || (row > 2^20)
    error('MATLAB:InvalidArgument', 'ERROR: row index must be a numeric scalar in range of 1 to 2^20!');
end
if ~isnumeric(col) || ~isscalar(col) || (col < 1) || (col > 2^14)
    error('MATLAB:InvalidArgument', 'ERROR: column index must be a numeric scalarin range of 1 to 2^14!.');
end
%% Convert index
% Calculate shifts (to handle changes like Z -> AA)
shift1 = fix((col - 1) / (BASE - 1));
shift2 = fix((shift1 - 1) / (BASE - 1));
col = col + shift1 + shift2 * BASE;
% Conversion to new base
numDigits = floor(log(col) / log(BASE)) + 1;
digits = zeros(1, numDigits);
iDigit = numDigits;
while col > 0
    digits(iDigit) = floor(mod(col, BASE));
    col = floor(col / BASE);
    iDigit = iDigit - 1;
end
% Conversion to characters
index = sprintf('%s%d', char(digits + double('A') - 1), row);
end
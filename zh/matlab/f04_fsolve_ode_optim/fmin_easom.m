clear;
close all;
% F�ggv�ny�rt�kek a megjelen�t�shez
d = -2:0.1:8;
[x, y] = meshgrid(d, d);
z = easom(x, y);
% Kezdeti becsl�s
X0 = [0; 0];
% Optimiz�land� f�ggv�ny
f = @(X)easom(X(1), X(2));
% Be�ll�t�sok
o = optimset('TolX', 1e-6, 'TolFun', 1e-6);
% Optimaliz�l�s futtat�sa
[Xmin, zmin] = fminsearch(f, X0, o);
disp(Xmin);
disp(zmin);
% Eredm�nyek megjelen�t�se
figure;
hold on;
surf(x, y, z);
scatter3(Xmin(1), Xmin(2), zmin, 'MarkerEdgeColor', 'red', 'MarkerFaceColor', 'red');
scatter3(-0.54719, -1.5479, -1.9133, 'MarkerEdgeColor', 'green', 'MarkerFaceColor', 'green');
xlabel('x');
ylabel('y');
zlabel('z');
title('Minimum of McCormic function');
view([19,17]);

% Booth f�ggv�ny
function z = easom(x, y)
z = -cos(x).*cos(y).*exp(-((x - pi).^2 + (y - pi).^2));
end

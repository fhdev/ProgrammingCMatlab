#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    printf("Egesz szam atvaltasa tetszoleges szamrendszerbe.\n");

    printf("Kerem adja meg az atvaltani kivant szamot!\n");
    printf("szam = ");
    int szam;
    scanf("%d", &szam);

	printf("Kerem adja meg a szamrendszer alapjat!\n");
    printf("alap = ");
    int alap;
    scanf("%d", &alap);

    printf("%d = ", szam);

    while(szam > 0)
	{
		int szamjegy = szam % alap;
		szam /= alap;
		printf("%d ", szamjegy);
	}

    return 0;
}

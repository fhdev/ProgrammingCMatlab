clear;
%% Aritmetikai oper�torok
u = [1, 2, 3]
v = [4; 5; 6]

%% �sszead�s �s kivon�s (azonos)
% M�trixokn�l dimenzi�, vektorokn�l elemsz�m 
% meg kell egyezzen
% sorvektor + oszlopvektor
upv1 = u + v % v + u azonos

% sorvektor + sorvektor
upv2 = u + v'

% oszlopvektor + oszlopvektor
upv3 = u' + v

%% Szorz�s
% Skal�r
um3 = u * 3
% M�trix
% sorvektor x oszlopvektor (skal�ris szorzat)
umv1 = u * v

% oszlopvektor x sorvektor (diadikus szorzat)
umv2 = v * u

% m�trix x m�trix
umv22 = umv2 * umv2

% Elemenk�nti
% sorvektor x sorvektor elemenk�nt
umv3 = u .* v'

% oszlopvektor x oszlopvektor elemenk�nt
umv4 = u' .* v

% m�trix x m�trix elemenk�nt
umv23 = umv2 .* umv2

%% Oszt�s
% Skal�r
d1 = 10 / 3

% "M�trix" -> line�ris egyenletrendszer megold�s
% xA = B egyenletrendszer megold�sa: B / A
A = randi(100, 3)
B = randi(100, 1, 3)
x1 = B / A

% Ax = B egyenletrendszer megold�sa: A \ B
x2 = A \ B'

% Elemenk�nt
% sorvektor / sorvektor elemenk�nt
udv3 = u ./ v'

% Hatv�nyoz�s
% M�trix
umv2_2m = umv2^3

% Elemenk�nti
umv2_2e = umv2 .^ 3

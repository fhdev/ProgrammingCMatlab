#include <stdio.h>
#include <ctype.h>

#define MIN 0
#define MAX 100

int main(int argc, char *argv[])
{
    printf("Szamkitalalo jatek\n");
    printf("Gondolj egy egesz szamra 0 es 100 kozott, es en kitalalom!\n");
    printf("Nyomj meg egy billentyut, ha keszen allsz!\n");
	getch();
	int hanytipp = 0;
	int also = MIN;
	int felso = MAX + 1;
	int tipp = 0;
	while(1)
	{
		hanytipp++;
		tipp = (also + felso) / 2;
		printf("\nTippelek! A tippem: %d. \n", tipp);
		printf("A szamod nagyobb [N], vagy kisebb [K], mint az enyem? Esetleg kitalaltam [T]? ");
		char valasz;
		scanf(" %c", &valasz);
		valasz = toupper(valasz);
		if(valasz == 'N')
		{
			also = tipp;
		}
		else if (valasz == 'K')
		{
			felso = tipp;
		}
		else if (valasz == 'T')
		{
			break;
		}
		else
		{
			printf("Inkabb megkerdezem ujra...\n");
		}
	}
	printf("\nA szamod valojaban a(z) %d volt. En %d probalkozasbol talaltam ki.", tipp, hanytipp);
    return 0;
}

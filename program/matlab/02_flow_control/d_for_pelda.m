%% Faktori�lis sz�m�t�s
n = 5;
f = 1;
for i=1:n
    f = f * i;
end
disp(f);
%% Szorz�t�bla k�sz�t�se
sz = zeros(10);
for i=1:10
    for j=1:10
        sz(i, j) = i * j;
    end
end
disp(sz);
%% Vektor elemein v�gig
for i = [2, 12, 26, 27, 43, 98]
    if mod(i, 2) == 0
        continue
    end
    disp(i);
    % Ciklus v�ltoz� v�ltoztat�sa nincs hat�ssal a
    % k�vetkez� fut�sra
    i = i + 1;    
end
%% M�trix oszlopain v�gig
for i = eye(3)
    disp(i);
end
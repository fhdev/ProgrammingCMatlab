% Random m�trix gener�l�sa (n x m) m�retben, mely csak p�ratlan elemekb�l �ll.
function A = randiodd(imax, n, m)
A = 2 * randi(imax, n, m) + 1;
end
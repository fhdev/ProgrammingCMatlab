% Find parameters of a mass-damper-spring system with unconsrained optimization
%% Clear workspace
clear;
close all;
clc;
%% Parameters as global variables
global m d s;
%% Load input and measurement
% Load default parameters and input
mds_param;
mds_input;
% Load measurement
load('meas.mat');
%% Create function handles for simulation
% Function handle of system.
sysfcn = @(t, X) mds_fcn(t, X, t_in, u_in);
% Options for ODE solution.
odeopts = odeset('MaxStep', t_step, 'InitialStep', t_step);
% Function handle of ODE solution.
odefcn = @() ode45(sysfcn, [t_start, t_stop], X_0, odeopts);
%% Simulate model with initial guess of parameter values
% Initial value of parameters p = [m; d; s].
p_0 = [1; 1; 1];
% Calculate initial values based on time-domain response
if false
    estim_mds; %#ok<UNRCH>
    p_0 = [m_meas, d_meas, s_meas];
end
% Simulate model with initial guess
m = p_0(1);
d = p_0(2);
s = p_0(3);
[t_p_0, X_p_0] = odefcn();
%% Find parameters of original model via optimalization
% Function handle of FMINSEARCH optimalization.
optimfcn = @(p) mds_costfcn_mincon(p, odefcn, t_out_0, X_meas);
% Options for FMINSEARCH optimalization.
optimopts = optimoptions('fmincon',                                     ...
                         'Algorithm',               'interior-point',   ...
                         'Display',                 'iter-detailed',    ... 
                         'MaxFunctionEvaluations',  500,                ...
                         'MaxIterations',           500,                ...
                         'PlotFcns',                @optimplotfval,     ...
                         'FunctionTolerance',       1e-6,               ...
                         'StepTolerance',           1e-6,               ...
                         'OptimalityTolerance',     1e-8,               ...
                         'ConstraintTolerance',     1e-6);
% Search for the parameter values at which the output of the system fits best to the measurement.
% No linear inequality and equality constraints are used
A = [];
b = [];
Aeq = [];
beq = [];
% Lower bound for parameter values
lb = 1e-3 * ones(1,3);
% Upper bound for parameter values
ub = 1e2 * ones(1,3);
% No nonlinear inequality and equality constraints are used
nonlcon = [];
% Run optimization
tic;
[p_opt, c_opt] = fmincon(optimfcn, p_0, A, b, Aeq, beq, lb, ub, nonlcon, optimopts);
toc;
%% Display results
% Display final parameter values
fprintf('m = %f, d = %f, s = %f\n', m, s, d);
% Calculate response with optimized parameters.
[t_out, X_out] = odefcn();
% Plot results
plot_optim;
#include <stdio.h>
#include <stdlib.h>

#include "lista.h"

#define CAST_BYTE(x) ((unsigned char *)(x))

static int lista_kapacitas(lista_t *lista, unsigned int kapacitas)
{
    if (kapacitas > lista->meret)
    {
        lista->meret *= 2;
        lista->adat = realloc(lista->adat, lista->meret * lista->elem_meret);
        if(lista->adat == NULL) return LISTA_HIBA;
    }
    return LISTA_OK;
}

int uj_lista(lista_t **lista, unsigned int meret, unsigned int elem_meret)
{
    if ((meret == 0) || (elem_meret == 0))
    {
        printf("LISTA_HIBA: Ervenytelen meret vagy elem meret!\n");
        return LISTA_HIBA;
    }
    (*lista) = malloc(sizeof(lista_t));
    if((*lista) == NULL)
    {
        printf("LISTA_HIBA: Sikertelen memoriafoglalas a lista szamara!\n");
        return LISTA_HIBA;
    }
    (*lista)->meret = meret;
    (*lista)->elem_meret = elem_meret;
    (*lista)->darab = 0;
    (*lista)->adat = malloc(meret * elem_meret);
    if((*lista)->adat == NULL)
    {
        printf("LISTA_HIBA: Sikertelen memoriafoglalas a listaelemek szamara!\n");
        torol_lista(lista);
        return LISTA_HIBA;
    }
    return LISTA_OK;
}

int torol_lista(lista_t **lista)
{
    if((*lista) != NULL)
    {
        if((*lista)->adat != NULL)
        {
            free((*lista)->adat);
            (*lista)->adat = NULL;
        }
        free(*lista);
        (*lista) = NULL;
    }
    return LISTA_OK;
}

int lista_elem_hozzaad(lista_t *lista, void *elem)
{
    if (lista_kapacitas(lista, lista->darab + 1) == LISTA_HIBA)
    {
        printf("LISTA_HIBA: Sikertelen memoriabovites a listaelemek szamara!\n");
        return LISTA_HIBA;
    }
    memmove(CAST_BYTE(lista->adat) + lista->darab * lista->elem_meret,
            elem,
            lista->elem_meret);
    lista->darab++;
    return LISTA_OK;
}

int lista_elem_lekerdez(lista_t *lista, void *elem, unsigned int index)
{
    if ((index < 0) || (index >= lista->darab))
    {
        printf("LISTA_HIBA: Hibas listaelem index!\n");
        return LISTA_HIBA;
    }
    memmove(elem,
            CAST_BYTE(lista->adat) + index * lista->elem_meret,
            lista->elem_meret);
    return LISTA_OK;
}


// indexek: 0 1 2      darab = 3
// �rt�kek: A B C
//
// t�rl�s: index = 0   darab - index - 1 = 2
//
// indexek: 0 1        darab = 2
// �rt�kek: B C
int lista_elem_torol(lista_t *lista, unsigned int index)
{
    if ((index < 0) || (index >= lista->darab))
    {
        printf("LISTA_HIBA: Hibas listaelem index!\n");
        return LISTA_HIBA;
    }
    memmove(CAST_BYTE(lista->adat) + index * lista->elem_meret,
            CAST_BYTE(lista->adat) + (index + 1) * lista->elem_meret,
            (lista->darab - index - 1) * lista->elem_meret);
    lista->darab--;
    return LISTA_OK;
}

// indexek: 0 1 2                    darab = 3
// �rt�kek: A B C
//
// besz�r�s: index = 0, �rt�k = X    darab - index = 3
//
// indexek: 0 1 2 3                  darab = 4
// �rt�kek: X A B C
int lista_elem_beszur(lista_t *lista, void *elem, unsigned int index)
{
    if ((index < 0) || (index >= lista->darab))
    {
        printf("LISTA_HIBA: Hibas listaelem index!\n");
        return LISTA_HIBA;
    }
    if (lista_kapacitas(lista, lista->darab + 1) == LISTA_HIBA)
    {
        printf("LISTA_HIBA: Sikertelen memoriabovites a listaelemek szamara!\n");
        return LISTA_HIBA;
    }
    memmove(CAST_BYTE(lista->adat) + (index + 1) * lista->elem_meret,
            CAST_BYTE(lista->adat) + index * lista->elem_meret,
            (lista->darab - index) * lista->elem_meret);
    memmove(CAST_BYTE(lista->adat) + index * lista->elem_meret,
            elem,
            lista->elem_meret);
    lista->darab++;
    return LISTA_OK;
}

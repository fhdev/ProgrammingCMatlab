% Egy m�trixban h�ny p�ros, �s h�ny p�ratlan elem van.
function [odd, even] = countoddeven(A)
odd = sum(mod(A(:), 2) == 1);
even = numel(A) - odd;
end
function outer6SigmaTest()
%% Test function
try
    numbers = ones(1, 1000);
    numbers(1) = 100;
    assert(outer6Sigma(numbers) == 1/1000, 'MATLAB:assertion:failed', 'Invalid value for vector input!');
        numbers = ones(100, 100);
    numbers(1) = 100;
    assert(outer6Sigma(numbers) == 1/10000, 'MATLAB:assertion:failed', 'Invalid value for vector input!');
        numbers = ones(10, 10, 10);
    numbers(1) = 100;
    assert(outer6Sigma(numbers) == 1/1000, 'MATLAB:assertion:failed', 'Invalid value for vector input!');
catch ex
    if strcmp(ex.identifier, 'MATLAB:assertion:failed')
        fprintf('%s\n', ex.message);
        return;
    else
        fprintf('Unexpected exception. Rethrowing... \n');
        throw(ex);
    end
end

%% Test passed
fprintf('%s\n', [mfilename, ' PASSED!']);

end
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// �ketezetes bet�k
#include <locale.h>
// Windows-on
#include <windows.h>

#define MAXARRAYLEN 1000
#define MIN -100.0
#define MAX  100.0

int main(int argc, char *argv[])
{
	// �kezetes bet�k
	setlocale(LC_ALL, "");
	// Windows-on
	SetConsoleCP(1250);
	SetConsoleOutputCP(1250);

    printf("V�letlen gener�lt sz�mok statisztik�ja.\n");

    // T�mbm�ret beolvas�sa
    printf("K�rem adja meg, h�ny darab sz�mot szeretne gener�lni!\n");
    printf("nmax = ");
    int nmax;
    scanf("%d", &nmax);
	nmax = (nmax > MAXARRAYLEN) ? MAXARRAYLEN : nmax;

	// Adatok gener�l�sa
	srand(time(0));
	double tomb[MAXARRAYLEN];
	for(int n = 0; n < nmax; n++)
	{
        tomb[n] = MIN + (MAX - MIN) * ((double)rand() / (double)RAND_MAX);
        printf("%+11.6lf\n", tomb[n]);
	}

    // �tlag sz�m�t�s
    double atlag = 0.0;
    for(int n = 0; n < nmax; n++)
	{
		atlag += tomb[n];
	}
	atlag /= nmax;
	printf("A sz�mok �tlaga: %+.6lf\n", atlag);

	// Sz�r�s sz�m�t�sa
    double szoras = 0.0;
    for(int n = 0; n < nmax; n++)
	{
		szoras += (tomb[n] - atlag) * (tomb[n] - atlag);
	}
	szoras /= (nmax - 1);
	szoras = sqrt(szoras);
	printf("A sz�mok sz�r�sa: %+.6lf\n", szoras);

    // Legnagyobb �s legkisebb elemek
    double legnagyobb = tomb[0];
    double legkisebb = tomb[0];
    for(int n = 1; n < nmax; n++)
	{
        if(tomb[n] > legnagyobb)
		{
			legnagyobb = tomb[n];
		}
		if(tomb[n] < legkisebb)
		{
			legkisebb = tomb[n];
		}
	}
	printf("A legnagyobb sz�m: %+.6lf\n", legnagyobb);
	printf("A legkisebb sz�m: %+.6lf\n", legkisebb);
    return 0;
}

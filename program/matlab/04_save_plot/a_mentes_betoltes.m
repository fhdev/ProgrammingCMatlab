% V�ltoz�k
M = rand(10);
v = rand(1, 10);
% Minden v�ltoz� ment�se MAT f�jlba
save('adatok.mat');
% T�rl�s a workspace-r�l
clear;
% F�jlban l�v� v�ltoz�k bet�lt�se
load('adatok.mat');
% M v�ltoz� ment�se sz�vegf�jlba
save('matrix.txt', 'M', '-ascii');
% M v�ltoz� t�rl�se
clear('M');
% M v�ltoz� visszat�lt�se sz�vegf�jlb�l
M = load('matrix.txt');
% M v�ltoz� ment�se Excel f�jlba
xlswrite('matrix.xlsx', M);
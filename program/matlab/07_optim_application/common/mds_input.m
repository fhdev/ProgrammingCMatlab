% Create input data for simulation of the motion of a mass-damper-spring system
%% Time of simulation
t_start = 0;
t_step  = 0.1;
t_stop  = 15;
%% Input data of simulation
% Time data
t_in = (t_start:t_step:t_stop)';
% Force excitation data (step to 1 at 1s, then step to -1 at 6s, then step back to 0 at 11s)
u_in = (t_in > 1.0) - 2 * (t_in > 6.0) + (t_in > 11.0);
%% Initial value of simulation
% X_0 = [ddx; dx]
X_0 = [0; 0];
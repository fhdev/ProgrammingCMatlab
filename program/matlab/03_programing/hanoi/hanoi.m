function [ ] = hanoi( topn, from, inter, to )
% Hanoi tornyai feladat rekurzív megvalósítása

if topn==1
    disp(['Disk 1 from ' num2str(from) ' to ' num2str(to)]);
    drawhanoi(from,to,1);
else
    hanoi(topn-1,from,to,inter);
    disp(['Disk ' num2str(topn) ' from ' num2str(from) ' to ' num2str(to)]);
    drawhanoi(from,to,1);
    hanoi(topn-1,inter,from,to);
end
end


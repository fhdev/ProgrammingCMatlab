function binary = decimal2octal(decimal)
if decimal == 0
    len = 1;
else
    len = floor(log(decimal)/log(8)) + 1;
end
binary = zeros(1, len);
while decimal > 0
    binary(len) = mod(decimal, 8);
    decimal = floor(decimal / 8);
    len = len - 1;
end
end
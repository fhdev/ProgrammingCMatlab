% Az e^x exponenci�lis kisz�m�t�sa Taylor-sorral.
function e = expt(x, nmax)
n = 0:nmax;
e = sum(x.^n ./ factorial(n));
end
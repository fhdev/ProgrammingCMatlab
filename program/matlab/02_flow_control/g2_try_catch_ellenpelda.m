clear;
clc;

struktura.mezo1 = 'alma';
struktura.mezo2 = 'k�rte';

mezoNev = 'mezo3';

% Try-Catch ellenp�lda: ne haszn�ljuk egyszer�en ellen�rizhet� potenci�lis hib�kra
try
    mezo = struktura.(mezoNev);
    fprintf('A %s mez� �rt�ke: %s\n', mezoNev, mezo);
catch
    fprintf('Nincs %s nev� mez�!\n', mezoNev);
end

% Helyesen
if isfield(struktura, mezoNev)
    mezo = struktura.(mezoNev);
    fprintf('A %s mez� �rt�ke: %s\n', mezoNev, mezo);
else
    fprintf('Nincs %s nev� mez�!\n', mezoNev);
end
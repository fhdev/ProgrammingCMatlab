% A cos(x) �r�tk�nek kisz�m�t�sa Taylor-sorral.
function c = cost(x, nmax)
n = 0:nmax;
c = sum((-1).^n ./ factorial(2*n) .* x.^(2 * n));
end
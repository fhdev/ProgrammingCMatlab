% Solve system of linear equations
%% Clear workspace
clear;
close all;
clc;
%% Ordinary square-size problem
% Solve A.x = b
A = [1, 2;
     2, 1];
b = [3;
     3];
x = A\b;
s = {num2str(A), ['  *  '; '     '], num2str(x), ['  =  '; '     '], num2str(b)};
disp('Solving fully determined linear equation system:');
disp([s{:}]);
%% Underdetermined system
% Obtain least-square solution for A.x = b
A = [2, 4, 0;
     0, 5, 10];
b = [ 8;
     20];
x = A\b;
disp('Solving underdetermined linear equation system:');
s = {[num2str(A); '         '],...
     ['  *  '; '     '; '     '],...
     num2str(x),...
     ['  =  '; '     '; '     '],...
     [num2str(b); '  ']};
disp([s{:}]);
%% Polynomial regression
x = [1, 5, 11, 19];
y = [1, 3, -4, 9];
% Vandermonde matrix
X = vander(x);
% Solution
p = X \ y';
% Plot
xx = x(1) : 0.1 : x(end);
yy = polyval(p, xx);
figure('Color', 'White');
hold on;
plot(x, y, 'ro', 'MarkerFaceColor', 'red');
plot(xx, yy, 'b-');
set(gca, 'XTick', x, 'YTick', sort(y), 'DefaultTextInterpreter', 'Latex', 'TickLabelInterpreter', 'Latex');
grid on;
box on;
title('Polynomial regression');
xlabel('x');
ylabel('y');
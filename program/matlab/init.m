if ~contains(path, cd)
    addpath(genpath(cd));
else
    rmpath(genpath(cd));
end